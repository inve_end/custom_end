#接口文档


###########################
#admin权限相关
###########################


#### 1。注册

**URI**: /admin/register/<br />
**METHOD**: POST<br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| name | int | Y | 账号 |
| password | string | Y | 密码 |
| pro_id | string | Y | 平台 ks=凯撒/ad=奥丁/yc=满贯|


**RESPONSE**:
```javascript
{
  "status": 0,
  "msg": "OK",
  "data": {
  }
}
```


### 2。角色管理
    屏蔽新增，删除按钮
    
    
### 3。用户管理
    用户新增
    增加字段 平台：pro_id string 




###########################
#业务相关
###########################


###功能一：问题类型配置

####1.list查询

**URI**: /api/problem_type/<br />
**METHOD**:GET<br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| page_no | int | Y | 页码 |
| page_size | int | Y | 每页条数 |


**RESPONSE**:
```javascript
{
    "data": {
        "items": [
            {
                "updated_at": "2019-06-05 16:03:13",
                "desc": "充值问题",
                "created_at": "2018-12-12 00:00:00",
                "image": "aaaa.jpg",
                "id": 1,
                "button": "充值申诉"
            },
            {
                "updated_at": "2018-12-12 00:00:00",
                "desc": "提现问题",
                "created_at": "2018-12-12 00:00:00",
                "image": "123312.png",
                "id": 2,
                "button": "提现反馈"
            },
            {
                "updated_at": "2018-12-12 00:00:00",
                "desc": "账号问题",
                "created_at": "2018-12-12 00:00:00",
                "image": "312312.png",
                "id": 3,
                "button": "账号反馈"
            },
            {
                "updated_at": "2018-12-12 00:00:00",
                "desc": "游戏问题",
                "created_at": "2018-12-12 00:00:00",
                "image": "12313.png",
                "id": 4,
                "button": "游戏反馈"
            },
            {
                "updated_at": "2018-12-12 00:00:00",
                "desc": "商务合作",
                "created_at": "2018-12-12 00:00:00",
                "image": "4123132.png",
                "id": 5,
                "button": "合作反馈"
            },
            {
                "updated_at": "2018-12-12 00:00:00",
                "desc": "其他问题",
                "created_at": "2018-12-12 00:00:00",
                "image": "432323.png",
                "id": 6,
                "button": "其他问题"
            }
        ],
        "total": 6
    },
    "status": 0,
    "timestamp": "2019-06-05 16:25:58",
    "msg": "OK"
}
```


####2.编辑

**URI**: /api/problem_type/<data_id>/<br />
**METHOD**: GET<br />
**PARAMS**:

**RESPONSE**:
```javascript
{
  "status": 0,
  "msg": "OK",
  "data": {
    "updated_at": "2018-12-12 00:00:00",
    "desc": "其他问题",
    "created_at": "2018-12-12 00:00:00",
    "image": "432323.png",
    "id": 6,
    "button": "其他问题"
  }
}
```


**URI**: /api/problem_type/<data_id>/<br />
**METHOD**: PUT<br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| desc | string | Y | 类型名称 |
| image | string | Y | 图片链接 |
| button | string | Y | 按钮名称 |

**RESPONSE**:
```javascript
{
  "status": 0,
  "msg": "OK",
  "data": {
    "id": 1
  }
}
```


###功能二：常见问题配置

####1.list查询

**URI**: /api/common_problem/<br />
**METHOD**: GET<br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| type_id | int | N | 问题类型|
| page_no | int | Y | 页码 |
| page_size | int | Y | 每页条数 |


**RESPONSE**:
```javascript
{
    "data": {
        "items": [
            {
                "updated_at": "2019-06-05 15:54:02",
                "title": "[1]充值未到账",
                "type_id": 1,
                "created_at": "2019-06-05 15:54:02",
                "id": 1,
                "answer": "充值后1分钟到账，若遇网络延迟，可能需要10-30分钟，请您耐心等待。 若 30分钟后仍未到账，您可在【充值记录】中针对未到账订单进行申诉。",
                "pro_id": "yc"
            },
            {
                "updated_at": "2019-06-05 15:55:14",
                "title": "[2]无法充值",
                "type_id": 1,
                "created_at": "2019-06-05 15:55:14",
                "id": 2,
                "answer": "若您在支付过程中发生错误，如交易中断、账号异常、支付关闭等，推荐您使用「VIP专属通道」和「官方代理充值」进行充值，官方保障，请您放心使用。",
                "pro_id": "yc"
            },
            {
                "updated_at": "2019-06-05 15:55:41",
                "title": "[3]代理充值是否可靠",
                "type_id": 1,
                "created_at": "2019-06-05 15:55:41",
                "id": 3,
                "answer": "「VIP专属通道」和「官方代理充值」均为官方认证的充值渠道，您可放心使用。",
                "pro_id": "yc"
            }
        ],
        "total": 3
    },
    "status": 0,
    "timestamp": "2019-06-05 16:30:46",
    "msg": "OK"
}
```


####2.新增


**URI**: /api/common_problem/<br />
**METHOD**: POST<br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| type_id | int | Y | 问题类型 |
| title | string | Y | 标题 |
| answer | string | Y | 答案 |


**RESPONSE**:
```javascript
{
  "status": 0,
  "msg": "OK",
  "data": {
  }
}
```

####3.编辑

**URI**: /api/common_problem/<data_id>/<br />
**METHOD**: GET<br />
**PARAMS**:

**RESPONSE**:
```javascript
{
  "status": 0,
  "msg": "OK",
  "data": {
        "updated_at": "2019-06-05 15:55:41",
        "title": "[3]代理充值是否可靠",
        "type_id": 1,
        "created_at": "2019-06-05 15:55:41",
        "id": 3,
        "answer": "「VIP专属通道」和「官方代理充值」均为官方认证的充值渠道，您可放心使用。",
        "pro_id": "yc"
  }
}
```


**URI**: /api/common_problem/<data_id>/<br />
**METHOD**: PUT<br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| type_id | int | Y | 问题类型 |
| title | string | Y | 标题 |
| answer | string | Y | 答案 |

**RESPONSE**:
```javascript
{
  "status": 0,
  "msg": "OK",
  "data": {
  }
}
```

####4.删除

**URI**: /api/common_problem/<data_id>/<br />
**METHOD**: DELETE<br />
**PARAMS**:

**RESPONSE**:
```javascript
{
  "status": 0,
  "msg": "OK",
  "data": {
  }
}
```



###功能三：客服常用语配置

####1.list查询

**URI**: /api/phrase/<br />
**METHOD**: GET<br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| type_id | int | N | 问题类型 |
| page_no | int | Y | 页码 |
| page_size | int | Y | 每页条数 |


**RESPONSE**:
```javascript
{
    "data": {
        "items": [
            {
                "updated_at": "2019-06-05 16:14:16",
                "temp_type": 1,
                "type_id": 3,
                "created_at": "2019-06-05 16:14:16",
                "user_id": null,
                "title": "输钱安抚",
                "context": "吃吃喝喝都是赔，唯有打牌有来回～",
                "id": 2,
                "pro_id": "yc"
            }
        ],
        "total": 1
    },
    "status": 0,
    "timestamp": "2019-06-05 16:38:43",
    "msg": "OK"
}
```

####2.新增

**URI**: /api/phrase/<br />
**METHOD**: POST<br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| type_id | int | Y | 问题类型 |
| temp_type | int | Y | 模版类型 |
| title | string | Y | 模版名称 |
| context | string | N | 模版内容 |

**RESPONSE**:
```javascript
{
  "status": 0,
  "msg": "OK",
  "data": {
  }
}
```

####3.编辑

**URI**: /api/phrase/<data_id>/<br />
**METHOD**: GET<br />
**PARAMS**:

**RESPONSE**:
```javascript
{
  "status": 0,
  "msg": "OK",
  "data": {
        "updated_at": "2019-06-05 16:14:16",
        "temp_type": 1,
        "type_id": 3,
        "created_at": "2019-06-05 16:14:16",
        "user_id": null,
        "title": "输钱安抚",
        "context": "吃吃喝喝都是赔，唯有打牌有来回～",
        "id": 2,
        "pro_id": "yc"
  }
}
```

**URI**: /api/phrase/<data_id>/<br />
**METHOD**: PUT<br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| type_id | int | Y | 问题类型 |
| temp_type | int | Y | 模版类型 |
| title | string | Y | 模版名称 |
| context | string | N | 模版内容 |

**RESPONSE**:
```javascript
{
  "status": 0,
  "msg": "OK",
  "data": {
  }
}
```

####4.删除

**URI**: /api/phrase/<data_id>/<br />
**METHOD**: DELETE<br />
**PARAMS**:

**RESPONSE**:
```javascript
{
  "status": 0,
  "msg": "OK",
  "data": {
  }
}
```



###功能四：客服公告配置

####1.list查询

**URI**: /api/announcement/<br />
**METHOD**: GET<br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| page_no | int | Y | 页码 |
| page_size | int | Y | 每页条数 |


**RESPONSE**:
```javascript
{
    "data": {
        "items": [
            {
                "updated_at": "2019-06-05 16:23:12",
                "end_at": "2019-06-20 00:00:00",
                "created_at": "2019-06-05 16:19:23",
                "chn": "chn1,chn2,chn3,chn4",
                "system": "ios,andirod",
                "start_at": "2019-06-01 00:00:00",
                "title": "致歉信",
                "context": "haha由于第三方转账出现异常，兑换功能暂时关闭。请各位老板放心，问题解决后将第一时间开放，请您耐心等待30分钟，感谢您的信任与支持！",
                "id": 1,
                "pkg": "com.test",
                "pro_id": "yc"
            }
        ],
        "total": 1
    },
    "status": 0,
    "timestamp": "2019-06-05 16:42:47",
    "msg": "OK"
}
```

####2.新增

**URI**: /api/announcement/<br />
**METHOD**: POST<br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| start_at | datetime | Y | 生效时间 |
| end_at | datetime | Y | 结束时间 |
| pkg | string | Y | 包名 |
| chn | string | Y | 渠道名 |
| system | string | Y | 收集系统 |
| title | string | Y | 公告标题 |
| context | string | Y | 公告内容 |

**RESPONSE**:
```javascript
{
  "status": 0,
  "msg": "OK",
  "data": {
  }
}
```

####3.编辑

**URI**: /api/announcement/<data_id>/<br />
**METHOD**: GET<br />
**PARAMS**:

**RESPONSE**:
```javascript
{
  "status": 0,
  "msg": "OK",
  "data": {
        "updated_at": "2019-06-05 16:23:12",
        "end_at": "2019-06-20 00:00:00",
        "created_at": "2019-06-05 16:19:23",
        "chn": "chn1,chn2,chn3,chn4",
        "system": "ios,andirod",
        "start_at": "2019-06-01 00:00:00",
        "title": "致歉信",
        "context": "haha由于第三方转账出现异常，兑换功能暂时关闭。请各位老板放心，问题解决后将第一时间开放，请您耐心等待30分钟，感谢您的信任与支持！",
        "id": 1,
        "pkg": "com.test",
        "pro_id": "yc"
  }
}
```

**URI**: /api/announcement/<data_id>/<br />
**METHOD**: PUT<br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| start_at | datetime | Y | 生效时间 |
| end_at | datetime | Y | 结束时间 |
| pkg | string | Y | 包名 |
| chn | string | Y | 渠道名 |
| system | string | Y | 收集系统 |
| title | string | Y | 公告标题 |
| context | string | Y | 公告内容 |

**RESPONSE**:
```javascript
{
  "status": 0,
  "msg": "OK",
  "data": {
  }
}
```
####4.删除

**URI**: /api/announcement/<data_id>/<br />
**METHOD**: DELETE<br />
**PARAMS**:

**RESPONSE**:
```javascript
{
  "status": 0,
  "msg": "OK",
  "data": {
  }
}
```


###功能五：充值申诉

####1.list查询

**URI**: /api/appeal/<br />
**METHOD**: GET<br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| page_no | int | Y | 页码 |
| page_size | int | Y | 每页条数 |
| data_id | int | N | 每页条数 |
| pay_id | int | N | 商户订单号 |
| player_id | int | N | 用户ID |
| pay_type | int | N | 支付分类 |
| appeal_type | int | N | 申诉渠道 |
| status | int | N | 申诉状态 |
| pay_name | string | N | 用户姓名/收款人姓名 |
| price_start | float | N | 充值金额 起 |
| price_end | float | N | 充值金额 止 |
| created_at_start | datetime | N | 申诉时间 起 |
| created_at_end | datetime | N | 申诉时间 止 |


**RESPONSE**:
```javascript
{
    "data": {
        "items": [
            {
               
            }
        ],
        "total": 1
    },
    "status": 0,
    "timestamp": "2019-06-05 16:42:47",
    "msg": "OK"
}
```

####2 .充值申诉详情

**URI**: /api/appeal/<data_id>/<br />
**METHOD**: GET<br />
**PARAMS**:


**RESPONSE**:
```javascript
{
    "data": {
        ...
    },
    "status": 0,
    "timestamp": "2019-06-05 16:42:47",
    "msg": "OK"
}
```

####3. 充值申诉处理接口（写备注，或者改申诉状态）

**URI**: /api/appeal/<data_id>/<br />
**METHOD**: PUT<br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| response | string | N | 回复内容 |
| status | int | N | 申诉状态 |
| remark | string | N | 备注 |

**RESPONSE**:



###功能六：客服回单

####1.list查询

**URI**: /api/customer_order/<br/>
**METHOD**: GET<br />
**PARAMS**: 

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| page_no | int | Y | 页码 |
| page_size | int | Y | 每页条数 |
| data_id | int | N | 工单ID |
| player_id | int | N | 用户ID |
| type_id | int | N | 问题类型 |
| status | int | N | 问题状态 |
| level | int | N | 用户等级 |
| created_at_start | datetime | N | 创建时间 起 |
| created_at_end | datetime | N | 创建时间 止 |


####2 .工单详情

**URI**: /api/work_order/<data_id>/<br />
**METHOD**: GET<br />
**PARAMS**:


**RESPONSE**:
```javascript
{
    "data": {
        ...
    },
    "status": 0,
    "timestamp": "2019-06-05 16:42:47",
    "msg": "OK"
}
```

####3. 工单处理

**URI**: /api/work_order/<data_id>/<br />
**METHOD**: PUT<br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| response | string | N | 回复内容 |
| is_solved | int | N | 是否解决 0=未解决 1=已解决 |
| status | int | N | 处理状态 0=未处理 1=已处理|
| remark | string | N | 备注 |

**RESPONSE**:


####4. 客服工作

**URI**: /admin/status/work/<br />
**METHOD**: PUT <br />
**PARAMS**:

**RESPONSE**:

####5. 客服休息

**URI**: /admin/status/rest/<br />
**METHOD**: PUT <br />
**PARAMS**:

**RESPONSE**:




###功能七：工单管理

####1.list查询

**URI**: /api/work_order/<br/>
**METHOD**: GET<br />
**PARAMS**: 

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| page_no | int | Y | 页码 |
| page_size | int | Y | 每页条数 |
| data_id | int | N | 工单ID |
| player_id | int | N | 用户ID |
| type_id | int | N | 问题类型 |
| is_solved | int | N | 是否解决 0=未解决 1=已解决 |
| status | int | N | 问题状态 |
| level | int | N | 用户等级 |
| handler_id | int | N | 处理人 |
| created_at_start | datetime | N | 申诉时间 起 |
| created_at_end | datetime | N | 申诉时间 止 |


####2 .查看工单详情

**URI**: /api/work_order/<data_id>/<br />
**METHOD**: GET<br />
**PARAMS**:


**RESPONSE**:
```javascript
{
    "data": {
        ...
    },
    "status": 0,
    "timestamp": "2019-06-05 16:42:47",
    "msg": "OK"
}
```


####3. 转接工单/获取所有在线客服

**URI**:/api/work_order/get_online_user/<br />
**METHOD**: GET<br />
**PARAMS**:


**RESPONSE**:
```javascript
{
    "data": {
        ...
    },
    "status": 0,
    "timestamp": "2019-06-05 16:42:47",
    "msg": "OK"
}
```


####4. 操作 转接工单

**URI**: /api/work_order/reassign/<data_id>/<br />
**METHOD**: PUT<br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| to_user_id | int | Y | 转接到的客服ID |

**RESPONSE**:


###功能八：转接记录

####1.list查询

**URI**: /api/reassign/<br/>
**METHOD**: GET<br />
**PARAMS**: 

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| page_no | int | Y | 页码 |
| page_size | int | Y | 每页条数 |
| order_id | int | N | 工单ID |
| from_user_id | int | N | 转接人 |
| to_user_id | int | N | 接收人 |
| operator_id | int | N | 操作人 |
| created_at_start | datetime | N | 转接时间 起 |
| created_at_end | datetime | N | 转接时间 止 |



###功能九：绩效管理

####1.list查询

**URI**: /api/performance/<br/>
**METHOD**: GET<br />
**PARAMS**: 

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| page_no | int | Y | 页码 |
| page_size | int | Y | 每页条数 |
| user_id | int | N | 操作者 |
| type_id | int | N | 问题类型 |
| day_start | date | N | 日期 起 2019-06-20|
| day_end | date | N | 日期 止 2019-06-21|

```javascript
{
    "data": {
        "items": [
            {
                "wait_count": 0,       待处理数量
                "comment_bad_count": 1,   不满意数量
                "avg_process_time": 14650,  平均处理时长
                "updated_at": "2019-06-21 17:53:30",
                "created_at": "2019-06-21 17:53:30",
                "user": {
                    "name": "kefu1"     操作者
                },
                "day": "2019-06-21",    日期
                "type": {
                    "desc": "账号问题"   问题类型
                },
                "user_id": 3, 
                "finish_count": 2,      已处理数量
                "type_id": 3,
                "comment_good_count": 0,   满意数量
                "id": 26,
                "good_percent": 0        好评率
            }
        ],
        "total": 1
    },
    "status": 0,
    "timestamp": "2019-06-05 16:42:47",
    "msg": "OK"
}
```


####2.汇总数据

**URI**: /api/performance/ttl/<br/>
**METHOD**: GET<br />
**PARAMS**: 

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| page_no | int | Y | 页码 |
| page_size | int | Y | 每页条数 |
| user_id | int | N | 操作者 |
| type_id | int | N | 问题类型 |
| day_start | date | N | 日期 起 2019-06-20|
| day_end | date | N | 日期 止 2019-06-21|

```
{
    "data": {
        "ttl_wait_count": 6,
        "ttl_comment_bad_count": 2,
        "ttl_avg_process_time": 3484.5833333333335,
        "ttl_finish_count": 12,
        "ttl_good_percent": 0.6666666666666666,
        "ttl_comment_good_count": 4
    },
    "timestamp": "2019-06-24 15:17:08",
    "msg": "OK",
    "status": 0
}
```

####3.操作人下拉框的数据

**URI**: /api/get_report_admin/<br/>
**METHOD**: GET<br />
**PARAMS**: 

```
{
    "data": [
        {
            "id": 3,
            "name": "kefu1"
        },
        {
            "id": 4,
            "name": "kefu2"
        },
        {
            "id": 5,
            "name": "kefu3"
        },
        {
            "id": 6,
            "name": "kefu4"
        },
        {
            "id": 7,
            "name": "kefu5"
        },
        {
            "id": 8,
            "name": "支付运营1"
        },
        {
            "id": 11,
            "name": "支付运营2"
        }
    ],
    "timestamp": "2019-06-24 15:24:43",
    "msg": "OK",
    "status": 0
}
```




###功能十：报表管理

####1.list查询

**URI**: /api/report/<br/>
**METHOD**: GET<br />
**PARAMS**: 

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| page_no | int | Y | 页码 |
| page_size | int | Y | 每页条数 |
| type_id | int | N | 问题类型 |
| second_type | int | N | 二级分类 |
| thrid_type | int | N | 三级分类 |
| day_start | date | N | 日期 起 2019-06-20|
| day_end | date | N | 日期 止 2019-06-21|

```javascript
{
    "data": {
        "items": [
            {
                "wait_count": 0,       待处理数量
                "total_count": 10,     反馈数量
                "avg_process_time": 14650,  平均处理时长
                "updated_at": "2019-06-21 17:53:30",
                "created_at": "2019-06-21 17:53:30",
                "day": "2019-06-21",    日期
                "type": {
                    "desc": "账号问题"   问题类型
                },
                "user_id": 3, 
                "finish_count": 2,      已处理数量
                "type_id": 3,
                "solved_count": 0,   已解决数量
                "unsolved_count": 0,   未解决数量
                "id": 26,
                "second_type": "闪退",
                "third_type": "金鲨银鲨"
            }
        ],
        "total": 1
    },
    "status": 0,
    "timestamp": "2019-06-05 16:42:47",
    "msg": "OK"
}
```


####2.汇总数据

**URI**: /api/report/ttl/<br/>
**METHOD**: GET<br />
**PARAMS**: 

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| page_no | int | Y | 页码 |
| page_size | int | Y | 每页条数 |
| type_id | int | N | 问题类型 |
| second_type | int | N | 二级分类 |
| thrid_type | int | N | 三级分类 |
| day_start | date | N | 日期 起 2019-06-20|
| day_end | date | N | 日期 止 2019-06-21|

```
{
    "data": {
        "ttl_solved_count": 0,    已解决数量
        "ttl_finish_count": 0,    已处理数量
        "ttl_unsolved_count": 0,  未解决数量
        "ttl_wait_count": 0,      未处理数量
        "ttl_count": 0            反馈数量
    },
    "timestamp": "2019-06-24 15:22:31",
    "msg": "OK",
    "status": 0
}
```

###功能十一：登陆申诉

####1.list查询

**URI**: /api/login_order/<br/>
**METHOD**: GET<br />
**PARAMS**: 

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| page_no | int | Y | 页码 |
| page_size | int | Y | 每页条数 |
| data_id | int | N | 工单ID |
| net | int | N | 网络 0=Wi-Fi 1=4G 2=3G 3=2G|
| contact_type | int | N | 联系方式 1=手机 2=微信 3=QQ |
| created_at_start | datetime | N | 申诉时间 起 |
| created_at_end | datetime | N | 申诉时间 止 |

```javascript
{
    "data": {
        "items": [
            {
                "remark": null,
                "appear_at": "2019-07-01 12:00:00",
                "updated_at": "2019-07-01 16:47:26",
                "id": 1,
                "extend": null,
                "pro_id": 3,
                "net": 0,
                "images": null,
                "contact_account": "13345678987",
                "status": 0,
                "phone_model": "huawei p30",
                "contact_type": 1,
                "desc": "hahahahahahuwqhdiqhddq和期待后期伟大的",
                "handler_id": null,
                "created_at": "2019-07-01 16:47:26"
            }
        ],
        "total": 1
    },
    "status": 0,
    "timestamp": "2019-06-05 16:42:47",
    "msg": "OK"
}
```


####2 .查看详情

**URI**: /api/login_order/<data_id>/<br />
**METHOD**: GET<br />
**PARAMS**:


**RESPONSE**:
```javascript
{
    "data": {
        "remark": null,
        "appear_at": "2019-07-01 12:00:00",
        "updated_at": "2019-07-01 16:47:26",
        "id": 1,
        "extend": null,
        "pro_id": 3,
        "net": 0,
        "images": null,
        "contact_account": "13345678987",
        "status": 0,
        "phone_model": "huawei p30",
        "contact_type": 1,
        "desc": "hahahahahahuwqhdiqhddq和期待后期伟大的",
        "handler_id": null,
        "created_at": "2019-07-01 16:47:26"
    },
    "status": 0,
    "timestamp": "2019-06-05 16:42:47",
    "msg": "OK"
}
```


####3. 处理

**URI**:/api/login_order/<data_id>/<br />
**METHOD**: PUT<br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| status | int | Y | 状态 0=待处理 1=处理中 2=已处理 |
| remark | string | N | 备注|



####  casiouser获取token的接口

**URI**: /rpc/get_token/<br/>
**METHOD**: GET<br />
**PARAMS**: 

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| user_id | int | Y | 玩家ID |
| pro_id | int | Y | 平台ID hj3=3|
| timestamp | int | Y | 时间戳 |
| sign | string | Y | 签名 |


```
{
    "data": {
        "token": '870d3ac0e66442619b1d3d99816617ff'
    },
    "timestamp": "2019-06-24 15:22:31",
    "msg": "OK",
    "status": 0
}
```
