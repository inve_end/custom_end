import hmac


AUTH_USER_HEADER_NAME = 'HMAC-USER'
AUTH_SIGN_HEADER_NAME = 'HMAC-SIGN'

AUTH_USER = 'lucas'
AUTH_SECRET = '22396fb7-59bf-44c2-a375-5c5a485892e6'


def generate_sign(params, secret):
    s = ''
    for k in sorted(params.keys()):
        if params[k] is None:
            continue
        s += '%s=%s&' % (k, params[k])
    s = s[:-1]
    m = hmac.new(secret.encode('utf8'), s.encode('utf8'))
    sign = m.hexdigest().upper()
    return sign


def get_headers(params):
    sign = generate_sign(params=params, secret=AUTH_SECRET)
    return {AUTH_USER_HEADER_NAME: AUTH_USER,
            AUTH_SIGN_HEADER_NAME: sign}
