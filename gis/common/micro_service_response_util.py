import json

from gis.common.exceptions import BizException, MICRO_SERVICE_NO_RESPONSE


def process_result(result):
    if result.status_code == 200:
        data = json.loads(result.content)
        status = data.get('status')
        message = data.get('msg')
        if status == 200:
            return data.get('data')
        else:
            raise BizException(status, message)
    else:
        raise BizException(MICRO_SERVICE_NO_RESPONSE)
