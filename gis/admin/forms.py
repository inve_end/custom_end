from marshmallow import fields
from gis.common.django_ext.forms import BaseSchema


class RegisterSchema(BaseSchema):
    name = fields.String(required=True)
    password = fields.String(required=True)
    pro_id = fields.Integer(required=True)


class LoginSchema(BaseSchema):
    name = fields.String(required=True)
    password = fields.String(required=True)
    pro_id = fields.Integer(required=True)


class AddUserSchema(BaseSchema):
    name = fields.String(required=True)
    password = fields.String(required=True)
    role_id = fields.Integer(required=True)
    enable = fields.Boolean(required=True)
    # pro_id = fields.Integer(required=True)


class UpdateUserSchema(BaseSchema):
    role_id = fields.Integer(required=True)
    enable = fields.Boolean(required=True)


class UpdateUserPasswordSchema(BaseSchema):
    old_password = fields.String(required=True)
    new_password = fields.String(required=True)


class RolePermissionRelSchema(BaseSchema):
    permission_id = fields.Integer(required=True)
    include_fields = fields.List(fields.String(required=True))


class AddRoleSchema(BaseSchema):
    name = fields.String(required=True)
    desc = fields.String()
    permissions = fields.List(fields.Nested(RolePermissionRelSchema))


class UpdateRoleSchema(BaseSchema):
    name = fields.String(required=True)
    desc = fields.String()
    permissions = fields.List(fields.Nested(RolePermissionRelSchema))
