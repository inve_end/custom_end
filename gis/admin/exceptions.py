from gis.common.exceptions import BizErrorCode

ERROR_USER_NOT_EXISTS = BizErrorCode(1001, '用户不存在')
ERROR_USER_PASSWORD_INCORRECT = BizErrorCode(1002, '密码不正确')
ERROR_USER_PASSWORD_DIFFERENT = BizErrorCode(1003, '密码两次设置不一样')
ERROR_USER_DISABLED = BizErrorCode(1004, '用户已被禁用')
ERROR_USER_INVALID_TOKEN = BizErrorCode(1005, '未授权用户')
ERROR_USER_NAME_DUPLICATE = BizErrorCode(1006, '用户名已存在')

ERROR_ROLE_NOT_EXISTS = BizErrorCode(1101, '角色不存在')
ERROR_ROLE_NAME_EXISTS = BizErrorCode(1101, '角色名称重复')
ERROR_ROLE_BIND_ONLY_LEAF_PERMISSION = BizErrorCode(1102, '绑定的权限具有子权限')
ERROR_ROLE_NOT_ALLOW_SET_PERMISSION_ATTR = BizErrorCode(1103, '该权限没有属性可设置')

ERROR_PERMISSION_NOT_EXISTS = BizErrorCode(1201, '权限不存在')
ERROR_PERMISSION_NOT_AUTHORIZED = BizErrorCode(1202, '未授权操作')

ERROR_USER_STATUS = BizErrorCode(1301, '用户状态错误')
ERROR_AUTH_TOKEN = BizErrorCode(1401, 'token错误')

