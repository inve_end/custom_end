import logging

from django.utils.decorators import method_decorator

from gis.admin import const
from gis.admin.decorators import require_login, check_permission
from gis.admin.forms import *
from gis.admin.services import admin_service
from gis.common.django_ext.decorators import validate_parameters
from gis.common.django_ext.forms import PaginationSchema
from gis.common.django_ext.views import BaseView, PaginationResponse

_LOGGER = logging.getLogger(__name__)


# 获取所有平台
class ProConfigListView(BaseView):
    @method_decorator(validate_parameters(PaginationSchema))
    def get(self, request, cleaned_data):
        total, ls = admin_service.list_pro_config(**cleaned_data)
        return PaginationResponse(total, ls)


# 客服接单
class StatusWorkView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('customer_change_status'))
    def put(self, request):
        admin_service.go_work(request.user_id)
        return dict(id=request.user_id)


# 客服休息
class StatusRestView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('customer_change_status'))
    def put(self, request):
        admin_service.go_rest(request.user_id)
        return dict(id=request.user_id)


#######################################
#######################################
# 当前登录用户操作
# 用户注册，登录，登出, 重置密码
class RegisterView(BaseView):
    @method_decorator(validate_parameters(RegisterSchema))
    def post(self, request, cleaned_data):
        user = admin_service.add_user(enable=False, **cleaned_data)
        return user


class LoginView(BaseView):
    @method_decorator(validate_parameters(LoginSchema))
    def post(self, request, cleaned_data):
        user = admin_service.verify_password(**cleaned_data)
        request.session[const.SESSION_USER_KEY] = user['id']
        return user


class LogoutView(BaseView):
    def post(self, request):
        try:
            del request.session[const.SESSION_USER_KEY]
        except KeyError:
            pass


class UserInfoView(BaseView):
    @method_decorator(require_login)
    def get(self, request):
        user = admin_service.get_user_by_id(request.user_id)
        user['permissions'] = list(admin_service.get_user_all_permission_codes(request.user_id))
        return user


class UserUpdatePasswordView(BaseView):
    @method_decorator(require_login)
    @method_decorator(validate_parameters(UpdateUserPasswordSchema))
    def put(self, request, cleaned_data):
        admin_service.reset_password_after_verify_old_success(request.user_id, **cleaned_data)


#######################################
#######################################
# 用户管理
class UserListView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('admin_user_get'))
    @method_decorator(validate_parameters(PaginationSchema))
    def get(self, request, cleaned_data):
        assert request.pro_id
        total, users = admin_service.list_users(pro_id=request.pro_id, **cleaned_data)
        return PaginationResponse(total, users)

    @method_decorator(require_login)
    @method_decorator(check_permission('admin_user_add'))
    @method_decorator(validate_parameters(AddUserSchema))
    def post(self, request, cleaned_data):
        assert request.user_id
        user = admin_service.add_user(pro_id=request.pro_id, **cleaned_data)
        return user


class UserDetailView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('admin_user_get'))
    def get(self, request, user_id):
        user = admin_service.get_user_by_id(user_id, check_enable=False)
        return user

    @method_decorator(require_login)
    @method_decorator(check_permission('admin_user_update'))
    @method_decorator(validate_parameters(UpdateUserSchema))
    def put(self, request, user_id, cleaned_data):
        admin_service.update_user(user_id, **cleaned_data)

    @method_decorator(require_login)
    @method_decorator(check_permission('admin_user_delete'))
    def delete(self, request, user_id):
        admin_service.delete_user(user_id)


class UserEnableView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('admin_user_enable'))
    def put(self, request, user_id):
        admin_service.enable_user(user_id)


class UserDisableView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('admin_user_disable'))
    def put(self, request, user_id):
        admin_service.disable_user(user_id)


#######################################
#######################################
# 角色管理
class RoleListView(BaseView):
    @method_decorator(require_login)
    # @method_decorator(check_permission('admin_role_get'))
    @method_decorator(validate_parameters(PaginationSchema))
    def get(self, request, cleaned_data):
        total, roles = admin_service.list_roles(**cleaned_data)
        return PaginationResponse(total, roles)

    # @method_decorator(require_login)
    # @method_decorator(check_permission('admin_role_add'))
    # @method_decorator(validate_parameters(AddRoleSchema))
    # def post(self, request, cleaned_data):
    #     role = admin_service.add_role(**cleaned_data)
    #     return role


class RoleDetailView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('admin_role_get'))
    def get(self, request, role_id):
        role = admin_service.get_role(role_id=role_id, with_permissions=True)
        return role

    @method_decorator(require_login)
    @method_decorator(check_permission('admin_role_update'))
    @method_decorator(validate_parameters(UpdateRoleSchema))
    def put(self, request, role_id, cleaned_data):
        admin_service.update_role(role_id, **cleaned_data)

    # @method_decorator(require_login)
    # @method_decorator(check_permission('admin_role_delete'))
    # def delete(self, request, role_id):
    #     admin_service.delete_role(role_id)


#######################################
#######################################
# 权限管理

class PermissionListView(BaseView):
    @method_decorator(require_login)
    def get(self, request):
        tree = admin_service.get_total_permission_tree()
        return dict(permissions=tree)
