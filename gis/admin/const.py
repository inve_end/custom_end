from enum import IntEnum, unique, Enum

SESSION_USER_KEY = 'user_id'

@unique
class UserStatus(IntEnum):
    """
    工单状态
    """
    REST = 0
    WORKING = 1
