import copy
import bcrypt
from uuid import uuid4
from django.db import transaction

from gis.admin.const import UserStatus
from gis.admin.exceptions import *
from gis.admin.models import User, Role, RolePermissionRel, Permission, ProConfig, AuthToken
from gis.common.django_ext.models import paginate
from gis.common.exceptions import BizException, ILLEGAL_PARAMETER


def list_pro_config(page_no, page_size):
    assert page_no > 0
    assert page_size > 0
    query = ProConfig.objects.all()
    return query.count(), [e.to_dict(exclude=['api_key']) for e in paginate(query, page_no, page_size)]


def get_pro_config(pro_id):
    assert pro_id
    pro = ProConfig.objects.filter(pk=pro_id).first()
    return pro.to_dict()


def go_work(user_id):
    user = User.objects.filter(pk=user_id).first()
    if user.status == UserStatus.WORKING:
        raise BizException(ERROR_USER_STATUS)
    user.status = UserStatus.WORKING
    user.save()


def go_rest(user_id):
    user = User.objects.filter(pk=user_id).first()
    if user.status == UserStatus.REST:
        raise BizException(ERROR_USER_STATUS)
    user.status = UserStatus.REST
    user.save()


def add_user(name, password, role_id=None, pro_id=None, enable=None):
    """
    新增用户
    """
    assert name
    assert password
    assert pro_id
    _check_password(password)
    _check_user_name_duplicate(name, pro_id)

    encrypted_pwd = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')
    with transaction.atomic():
        user = User.objects.create(name=name, password=encrypted_pwd, pro_id=pro_id, role_id=role_id, enable=enable)

    return user.to_dict(exclude=['password', 'deleted'])


def delete_user(user_id):
    """
    删除用户
    """
    assert isinstance(user_id, int)
    with transaction.atomic():
        user = _get_user_by_id(user_id, check_enable=False)
        user.deleted = True
        user.enable = False
        user.save()


def update_user(user_id, role_id=None, enable=True):
    """
    修改用户信息，包括重新分配角色
    """
    with transaction.atomic():
        user = _get_user_by_id(user_id, check_enable=False)
        user.role_id = role_id
        user.enable = enable
        user.save()


def get_user_by_id(user_id, check_enable=True):
    user = _get_user_by_id(user_id, check_enable=check_enable)
    result = user.to_dict(exclude=['password'])
    return result


def list_users(pro_id, page_no, page_size, status=None, role_id=None):
    query = User.objects.filter(deleted=False).filter(pro_id=pro_id)
    if status is not None:
        query = query.filter(status=status)
    if role_id:
        query = query.filter(role_id=role_id)
    users = paginate(query, page_no, page_size)
    resp_item = []
    for user in users:
        item = user.to_dict(exclude=['password', 'deleted'])
        item['role'] = user.role.to_dict(fields=['id', 'name']) if user.role else {}
        resp_item.append(item)
    return query.count(), resp_item


def disable_user(user_id):
    """
    禁用用户
    """
    user = _get_user_by_id(user_id)
    user.enable = False
    user.save()


def enable_user(user_id):
    """
    启用用户
    """
    user = _get_user_by_id(user_id, check_enable=False)
    user.enable = True
    user.save()


def verify_password(name, password, pro_id):
    """
    验证密码正确性
    """
    user = _get_user_by_name(name, pro_id)
    if not user:
        raise BizException(ERROR_USER_NOT_EXISTS)
    if not bcrypt.checkpw(password.encode('utf-8'), user.password.encode('utf-8')):
        raise BizException(ERROR_USER_PASSWORD_INCORRECT)
    return user.to_dict(exclude=['password', 'deleted'])


def reset_password_after_verify_old_success(user_id, old_password, new_password):
    """
    更新密码, 需先检验旧密码
    """
    user = _get_user_by_id(user_id)
    if not bcrypt.checkpw(old_password.encode('utf-8'), user.password.encode('utf-8')):
        raise BizException(ERROR_USER_PASSWORD_INCORRECT)
    reset_password(user_id, new_password)


def reset_password(user_id, password):
    """
    重置密码
    """
    _check_password(password)
    user = _get_user_by_id(user_id)
    if bcrypt.checkpw(password.encode('utf-8'), user.password.encode('utf-8')):
        raise BizException(ERROR_USER_PASSWORD_DIFFERENT)
    encrypted_pwd = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')
    user.password = encrypted_pwd
    user.save()


def get_role_id_by_id(user_id):
    user = _get_user_by_id(user_id)
    role = user.roles.first()
    return role.id


def get_user_name_group_by_id(user_id, include_self=True, include_children=False):
    user = _get_user_by_id(user_id)
    user_name_list = [user.name] if include_self else []
    if include_children:
        children = User.objects.filter(parent=user).all()
        for child in children:
            user_name_list.append(child.name)
    return user_name_list


def _get_user_by_id(user_id, check_deleted=True, check_enable=True):
    user = User.objects.filter(pk=user_id, deleted=False).first()
    _check_user_valid(user, check_deleted, check_enable)
    return user


def _get_user_by_name(name, pro_id, check_deleted=True, check_enable=True):
    user = User.objects.filter(name=name).filter(pro_id=pro_id).first()
    _check_user_valid(user, check_deleted, check_enable)
    return user


def _check_user_name_duplicate(name, pro_id):
    if User.objects.filter(name=name).filter(pro_id=pro_id).exists():
        raise BizException(ERROR_USER_NAME_DUPLICATE)


def _check_user_valid(user, check_deleted, check_enable):
    if not user:
        raise BizException(ERROR_USER_NOT_EXISTS)
    if check_deleted and user.deleted:
        raise BizException(ERROR_USER_NOT_EXISTS)
    if check_enable and not user.enable:
        raise BizException(ERROR_USER_DISABLED)


def _check_password(password):
    errors = []
    if not any(x.isupper() for x in password):
        errors.append('必需含有大写字母\n')
    if not any(x.islower() for x in password):
        errors.append('必需含有小写字母\n')
    if not any(x.isdigit() for x in password):
        errors.append('必需含有数字\n')
    if not len(password) >= 6:
        errors.append('长度必需大于6\n')
    if errors:
        raise BizException(ILLEGAL_PARAMETER, ''.join(errors))


##################################################
##################################################

def add_role(name, desc=None, permissions=None):
    """
    添加角色，并绑定权限
    """
    _check_role_permission_rel(permissions)
    _check_role_name_duplicate(name)

    with transaction.atomic():
        role = Role.objects.create(name=name, desc=desc)
        _bind_role_and_permissions(role, permissions)

    return role.to_dict()


def _check_role_name_duplicate(name):
    if Role.objects.filter(name=name).exists():
        raise BizException(ERROR_ROLE_NAME_EXISTS)


def delete_role(role_id):
    """
    删除角色，会删除该角色绑定的权限关系
    """
    role = _get_role(role_id)
    deleted_count, _ = role.delete()
    return deleted_count > 0


def update_role(role_id, name, desc=None, permissions=None):
    """
    更新角色，包括更新权限信息
    """
    _check_role_permission_rel(permissions)
    role = _get_role(role_id)
    with transaction.atomic():
        role.name = name
        role.desc = desc
        role.save()
        # 重新设置权限，目前用的暴力删除全部旧的再重新设置。
        # 可以优化：分成新增，修改，删除三个集合事件，分别处理
        role.permissions.clear()
        _bind_role_and_permissions(role, permissions)


def get_role(role_id, with_permissions=False):
    """
    返回角色信息, 如果 with_permissions 为True时，返回数据包含权限列表
    :return {'permission': list<RolePermissionBO>}
    """
    role = _get_role(role_id)
    result = role.to_dict()
    if with_permissions:
        permissions = RolePermissionRel.objects.filter(role=role)
        result['permissions'] = [e.to_dict(fields=['permission', 'include_fields']) for e in permissions]
    return result


def list_roles(page_no, page_size):
    query = Role.objects.all()
    return query.count(), [e.to_dict() for e in paginate(query, page_no, page_size)]


def _bind_role_and_permissions(role, permissions):
    if not permissions:
        return
    for each in permissions:
        permission_id = each.get('permission_id')
        include_fields = each.get('include_fields')
        permission = _get_permission(permission_id)
        if not permission.is_leaf:
            raise BizException(ERROR_ROLE_BIND_ONLY_LEAF_PERMISSION, permission_id)
        if not permission.fields and include_fields:
            raise BizException(ERROR_ROLE_NOT_ALLOW_SET_PERMISSION_ATTR, permission_id)
        RolePermissionRel.objects.create(role=role, permission=permission, include_fields=include_fields)


def _check_role_permission_rel(permissions):
    if permissions:
        assert isinstance(permissions, list)


def _get_role(role_id):
    role = Role.objects.filter(pk=role_id).first()
    if not role:
        raise BizException(ERROR_ROLE_NOT_EXISTS, role_id)
    return role


##################################################
##################################################

def get_user_permission_include_fields(user_id, permission_code):
    """
    返回某个用户对特定某个权限的可见字段集合
    :param user_id:
    :param permission_code:
    :return: {field1, field2, ...}
    """
    user = _get_user_by_id(user_id)
    result = set()
    if user.is_super:
        permission = Permission.objects.get(code=permission_code)
        if permission.fields:
            result = set(permission.fields)
    else:
        rels = RolePermissionRel.objects.filter(role__user__id=user_id, permission__code=permission_code)
        for each in rels:
            if each.include_fields:
                result.update(set(each.include_fields))
    return result


def get_user_all_permission_codes(user_id):
    """
    返回用户权限列表
    如果用户拥有一个 1/2/3/ 这样的三级结点权限，则会1，2，3级结点权限编码都会返回
    如果用户拥有多个角色，权限为所有角色的并集

    :param user_id:
    :return: {permission_code, ...}
    """
    result = set()
    user = _get_user_by_id(user_id)
    if user.is_super:
        result = {e.code for e in Permission.objects.all()}
    else:
        role = user.role
        permissions = role.permissions.all()
        for permission in permissions:
            for each in _get_ancestor_include_current_permission(permission):
                result.add(each['code'])

    return result


def get_total_permission_tree():
    """
    返回整个权限树结构视图
    """
    result = []
    index_map = copy.deepcopy(get_permission_index_map_cache())
    for pk, each in index_map.items():
        if each['parent_id']:
            parent = index_map[each['parent_id']]
            if 'children' in parent:
                parent['children'].append(index_map[pk])
            else:
                parent['children'] = [index_map[pk]]
        else:
            result.append(index_map[pk])
    return result


ALL_PERMISSION_CACHE = None


def get_permission_index_map_cache():
    global ALL_PERMISSION_CACHE
    if ALL_PERMISSION_CACHE:
        return ALL_PERMISSION_CACHE
    all_permissions = list(Permission.objects.all())
    ALL_PERMISSION_CACHE = {e.pk: e.to_dict(fields=['id', 'parent', 'name', 'code', 'fields']) for e in all_permissions}
    return ALL_PERMISSION_CACHE


def _get_permission(permission_id):
    permission = Permission.objects.filter(pk=permission_id).first()
    if not permission:
        raise BizException(ERROR_PERMISSION_NOT_EXISTS, permission_id)
    return permission


def _get_ancestor_include_current_permission(permission):
    ancestor_ids = [int(e) for e in permission.full_path.split('/') if e]
    return [get_permission_index_map_cache()[e] for e in ancestor_ids]


##################################################
##################################################

def check_auth_token(user_id, token, pro_id):
    auth_token = AuthToken.objects.filter(pro_id=pro_id).filter(user_id=user_id).filter(token=token).first()
    return True if auth_token else False


def set_auth_token(user_id, pro_id):
    auth_token = AuthToken.objects.filter(pro_id=pro_id).filter(user_id=user_id).first()
    if auth_token:
        auth_token.delete()
    token_string = uuid4().hex
    AuthToken.objects.create(pro_id=pro_id, user_id=user_id, token=token_string)
    return token_string
