#部署文档

##一、数据库相关
    1. 创建mysql数据库  dbname = thanos
       执行建表及初始数据语句   init.sql


##二、配置相关
1. supervisor增加配置  
`nasa.supervisor.conf`
2. nginx增加配置    
`nasa.nginx.conf`
3. cron任务
`crontab.conf`
4. 导出文件存放路径  /tmp/export_thanos/     请建好该文件夹


##三、初始化
部署路径  /opt/thanos/enabled/app
依赖于python3， 请安装python3环境
创建thanos虚拟环境

