CREATE TABLE `admin_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  `sort_index` int(11) NOT NULL,
  `desc` varchar(200) DEFAULT NULL,
  `full_path` varchar(100) NOT NULL,
  `is_leaf` tinyint(1) NOT NULL,
  `fields` varchar(1000) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `admin_permission_parent_id_a4f457b3_fk_admin_permission_id` (`parent_id`),
  CONSTRAINT `admin_permission_parent_id_a4f457b3_fk_admin_permission_id` FOREIGN KEY (`parent_id`) REFERENCES `admin_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `admin_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `desc` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `admin_rolepermissionrel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `include_fields` varchar(1000) DEFAULT NULL,
  `permission_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_rolepermissionrel_role_id_permission_id_e415d854_uniq` (`role_id`,`permission_id`),
  KEY `admin_rolepermission_permission_id_fa4d6875_fk_admin_per` (`permission_id`),
  CONSTRAINT `admin_rolepermission_permission_id_fa4d6875_fk_admin_per` FOREIGN KEY (`permission_id`) REFERENCES `admin_permission` (`id`),
  CONSTRAINT `admin_rolepermissionrel_role_id_39e41936_fk_admin_role_id` FOREIGN KEY (`role_id`) REFERENCES `admin_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `admin_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `is_super` tinyint(1) NOT NULL,
  `enable` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `pro_id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `admin_user_role_id_b65ad33e_fk_admin_role_id` (`role_id`),
  KEY `admin_user_pro_id_08c39c62` (`pro_id`),
  CONSTRAINT `admin_user_role_id_b65ad33e_fk_admin_role_id` FOREIGN KEY (`role_id`) REFERENCES `admin_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `announcement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `start_at` datetime(6) NOT NULL,
  `end_at` datetime(6) NOT NULL,
  `pkg` varchar(500) NOT NULL,
  `chn` varchar(500) NOT NULL,
  `system` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `context` varchar(1000) NOT NULL,
  `pro_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `announcement_pro_id_db86fb40` (`pro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `auth_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `pro_id` smallint(6) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `token` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `common_problem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `title` varchar(200) NOT NULL,
  `answer` varchar(1000) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `common_problem_pro_id_92c48636` (`pro_id`),
  KEY `common_problem_type_id_eaf03ab4` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `performance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `day` date NOT NULL,
  `finish_count` int(11) NOT NULL,
  `wait_count` int(11) NOT NULL,
  `comment_good_count` int(11) NOT NULL,
  `comment_bad_count` int(11) NOT NULL,
  `good_percent` double NOT NULL,
  `avg_process_time` double NOT NULL,
  `type_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `performance_type_id_961b1ce8` (`type_id`),
  KEY `performance_user_id_8281b907` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `phrase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `temp_type` smallint(6) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `context` varchar(1000) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `phrase_pro_id_69e1f68f` (`pro_id`),
  KEY `phrase_type_id_b42cec3c` (`type_id`),
  KEY `phrase_user_id_609346da` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `pro_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `api_key` varchar(100) NOT NULL,
  `get_data_url` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `problem_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `desc` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `button` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `reassign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `player_id` bigint(20) NOT NULL,
  `stay_time` int(11) NOT NULL,
  `from_user_id` int(11) NOT NULL,
  `operator_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `to_user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `reassign_from_user_id_0b3359ea` (`from_user_id`),
  KEY `reassign_operator_id_4dcd2da1` (`operator_id`),
  KEY `reassign_order_id_26916c1d` (`order_id`),
  KEY `reassign_pro_id_4b78b964` (`pro_id`),
  KEY `reassign_to_user_id_28154f52` (`to_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `day` date NOT NULL,
  `second_type` int(11) DEFAULT NULL,
  `third_type` int(11) DEFAULT NULL,
  `total_count` int(11) NOT NULL,
  `finish_count` int(11) NOT NULL,
  `wait_count` int(11) NOT NULL,
  `solved_count` int(11) NOT NULL,
  `unsolved_count` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `pro_id` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `report_type_id_b5f1b9fa` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE `work_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `status` smallint(6) NOT NULL,
  `finish_at` datetime(6) DEFAULT NULL,
  `is_solved` smallint(6) DEFAULT NULL,
  `reassign_at` datetime(6) DEFAULT NULL,
  `urgent_count` smallint(6) NOT NULL,
  `urgent_at` datetime(6) DEFAULT NULL,
  `process_time` int(11) DEFAULT NULL,
  `player_id` bigint(20) NOT NULL,
  `level` smallint(6) NOT NULL,
  `chn` varchar(200) NOT NULL,
  `game_version` varchar(200) NOT NULL,
  `ua` varchar(200) NOT NULL,
  `platform` varchar(200) NOT NULL,
  `game` smallint(6) DEFAULT NULL,
  `question_type` smallint(6) DEFAULT NULL,
  `desc` varchar(1000) DEFAULT NULL,
  `appear_at` datetime(6) DEFAULT NULL,
  `images` varchar(1000) DEFAULT NULL,
  `withdraw_id` bigint(20) DEFAULT NULL,
  `withdraw_at` datetime(6) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `appeal_type` smallint(6) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `pay_id` bigint(20) DEFAULT NULL,
  `pay_type` smallint(6) DEFAULT NULL,
  `pay_name` varchar(50) DEFAULT NULL,
  `pay_at` datetime(6) DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  `response` varchar(1000) DEFAULT NULL,
  `cooperation` text,
  `comment` smallint(6) DEFAULT NULL,
  `extend` text,
  `checked` smallint(6) DEFAULT NULL,
  `handler_id` int(11) DEFAULT NULL,
  `pro_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `work_order_handler_id_aa6f4999` (`handler_id`),
  KEY `work_order_pro_id_87c9b69b` (`pro_id`),
  KEY `work_order_type_id_6e583a9f` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `login_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `pro_id` smallint(6) NOT NULL,
  `status` smallint(6) NOT NULL,
  `appear_at` datetime(6) DEFAULT NULL,
  `net` smallint(6) NOT NULL,
  `phone_model` varchar(200) DEFAULT NULL,
  `contact_type` smallint(6) NOT NULL,
  `contact_account` varchar(200) DEFAULT NULL,
  `desc` varchar(1000) DEFAULT NULL,
  `images` varchar(1000) DEFAULT NULL,
  `remark` longtext,
  `extend` longtext,
  `handler_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `login_order_handler_id_2bef614e` (`handler_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--    pro_config
INSERT INTO `pro_config` (`id`, `created_at`, `updated_at`, `name`, `api_key`)
VALUES
(1,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','凯撒','asdd12313edqdqdw11d'),
(2,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','奥丁','sdce2d1231231qdwddd'),
(3,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','满贯','cwdcd21312eddwqdwee');


--   permission初始数据
INSERT INTO `admin_permission` (`id`, `created_at`, `updated_at`, `name`, `code`, `sort_index`, `desc`, `full_path`, `is_leaf`, `fields`, `parent_id`)
VALUES
	(1,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','控制权限管理','admin_group',5,NULL,'1/',0,NULL,NULL),
	(2,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','用户管理','admin_user_group',1,NULL,'1/2/',0,NULL,1),
	(3,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','新增用户','admin_user_add',1,NULL,'1/2/3/',1,NULL,2),
	(4,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','修改用户','admin_user_update',2,NULL,'1/2/4/',1,NULL,2),
	(5,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','删除用户','admin_user_delete',3,NULL,'1/2/5/',1,NULL,2),
	(6,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','查看用户','admin_user_get',4,NULL,'1/2/6/',1,NULL,2),
	(7,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','禁用用户','admin_user_disable',5,NULL,'1/2/7/',1,NULL,2),
	(8,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','启用用户','admin_user_enable',6,NULL,'1/2/8/',1,NULL,2),
	(50,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','角色管理','admin_role_group',2,NULL,'1/50/',0,NULL,1),
	(51,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','添加角色','admin_role_add',1,NULL,'1/50/51/',1,NULL,50),
	(52,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','修改角色','admin_role_update',2,NULL,'1/50/52/',1,NULL,50),
	(53,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','删除角色','admin_role_delete',3,NULL,'1/50/53/',1,NULL,50),
	(54,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','查看角色','admin_role_get',4,NULL,'1/50/54/',1,NULL,50),
    (1000,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','配置管理','config_group',1,NULL,'1000/',0,NULL,NULL),
	(1001,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','问题类型配置','problem_type',1,NULL,'1000/1001/',0,NULL,1000),
	(1002,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','问题类型配置查询','problem_type_get',1,NULL,'1000/1001/1002/',1,NULL,1001),
	(1003,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','问题类型配置修改','problem_type_mod',2,NULL,'1000/1001/1003/',1,NULL,1001),
	(1101,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','常见问题配置','common_problem',2,NULL,'1000/1101/',0,NULL,1000),
	(1102,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','常见问题配置查询','common_problem_get',1,NULL,'1000/1101/1102/',1,NULL,1101),
	(1103,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','常见问题配置新增','common_problem_add',2,NULL,'1000/1101/1103/',1,NULL,1101),
	(1104,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','常见问题配置修改','common_problem_mod',3,NULL,'1000/1101/1104/',1,NULL,1101),
	(1105,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','常见问题配置删除','common_problem_del',4,NULL,'1000/1101/1105/',1,NULL,1101),
	(1201,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','客服常用语配置','phrase',3,NULL,'1000/1201/',0,NULL,1000),
	(1202,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','客服常用语配置查询','phrase_get',1,NULL,'1000/1201/1202/',1,NULL,1201),
	(1203,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','客服常用语配置新增','phrase_add',2,NULL,'1000/1201/1203/',1,NULL,1201),
	(1204,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','客服常用语配置修改','phrase_mod',3,NULL,'1000/1201/1204/',1,NULL,1201),
	(1205,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','客服常用语配置删除','phrase_del',4,NULL,'1000/1201/1205/',1,NULL,1201),
	(1301,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','客服公告配置','announcement',4,NULL,'1000/1301/',0,NULL,1000),
	(1302,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','客服公告配置查询','announcement_get',1,NULL,'1000/1301/1302/',1,NULL,1301),
	(1303,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','客服公告配置新增','announcement_add',2,NULL,'1000/1301/1303/',1,NULL,1301),
	(1304,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','客服公告配置修改','announcement_mod',3,NULL,'1000/1301/1304/',1,NULL,1301),
	(1305,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','客服公告配置删除','announcement_del',4,NULL,'1000/1301/1305/',1,NULL,1301),
    (2000,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','工单管理','order_group',2,NULL,'2000/',0,NULL,NULL),
	(2001,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','充值申诉','appeal',1,NULL,'2000/2001/',0,NULL,2000),
	(2002,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','充值申诉查询','appeal_get',1,NULL,'2000/2001/2002/',1,NULL,2001),
    (2003,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','充值申诉处理','appeal_mod',2,NULL,'2000/2001/2003/',1,NULL,2001),
	(2101,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','客服回单','customer_order',2,NULL,'2000/2101/',0,NULL,2000),
	(2102,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','客服回单查询','customer_order_get',1,NULL,'2000/2101/2102/',1,NULL,2101),
    (2103,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','客服回单处理','customer_order_mod',2,NULL,'2000/2101/2103/',1,NULL,2101),
    (2104,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','客服接单/休息','customer_change_status',3,NULL,'2000/2101/2104/',1,NULL,2101),
	(2201,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','工单管理','work_order',3,NULL,'2000/2201/',0,NULL,2000),
	(2202,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','工单管理查询','work_order_get',1,NULL,'2000/2201/2202/',1,NULL,2201),
    (2203,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','客服回单转接','work_order_reassign',2,NULL,'2000/2201/2203/',1,NULL,2201),
    (2301,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','转接记录','reassign',4,NULL,'2000/2301/',0,NULL,2000),
	(2302,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','转接记录查询','reassign_get',1,NULL,'2000/2301/2302/',1,NULL,2301)
	(2401,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','登陆申诉','login_order',5,NULL,'2000/2401/',0,NULL,2000),
	(2402,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','登陆申诉查询','login_order_get',1,NULL,'2000/2401/2402/',1,NULL,2401),
    (2403,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','登陆申诉处理','login_order_mod',2,NULL,'2000/2401/2403/',1,NULL,2401),
    (3000,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','数据统计','data_group',3,NULL,'3000/',0,NULL,NULL),
	(3001,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','报表管理','report',1,NULL,'3000/3001/',0,NULL,3000),
	(3002,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','报表管理查询','report_get',1,NULL,'3000/3001/3002/',1,NULL,3001),
	(3003,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','报表管理导出','report_dowmload',2,NULL,'3000/3001/3003/',1,NULL,3001),
	(3101,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','绩效管理','performance',2,NULL,'3000/3101/',0,NULL,3000),
	(3102,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','绩效管理查询','performance_get',1,NULL,'3000/3101/3102/',1,NULL,3101),
	(3103,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','绩效管理导出','performance_download',2,NULL,'3000/3101/3103/',1,NULL,3101);


--  角色初始化
INSERT INTO `admin_role` (`id`, `created_at`, `updated_at`, `name`, `desc`)
VALUES
	(1,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','客服',NULL),
	(2,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','客服主管',NULL),
	(3,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','运营',NULL),
	(4,'2018-12-12 00:00:00.000000','2018-12-12 00:00:00.000000','支付运营',NULL);


--  user初始数据，超级管理员admin---123456
INSERT INTO `admin_user` (`id`, `created_at`, `updated_at`, `name`, `password`, `is_super`, `enable`, `deleted`, `pro_id`)
VALUES
	(1,'2018-01-01 00:00:00.000000','2018-01-01 00:00:00.000000','admin','$2b$12$U2TK8u4Smdmzan20xT9Ocu3tcEamTHHZ4RNaS9BOwZUsMwvghmq2i',1,1,0,3);

