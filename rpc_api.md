#接口文档


###########################
#h5接口相关
###########################


####1.获取问题类型

**URI**: /rpc/problem_type/<br />
**METHOD**:GET<br />
**PARAMS**:

**RESPONSE**:


####2.获取常见问题

**URI**: /rpc/common_problem/<br />
**METHOD**:GET<br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| pro_id | int | Y | 平台id（凯撒=1，奥丁=2，满贯=3） |
| type_id | int | Y | 问题类型 |

**RESPONSE**:


####3.获取公告

**URI**: /rpc/announcement/<br />
**METHOD**:GET<br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| pro_id | int | Y | 平台id（凯撒=1，奥丁=2，满贯=3） |
| pkg | string | Y | 包名 |
| chn | string | Y | 渠道名 |

**RESPONSE**:


####4.获取工单记录

**URI**: /rpc/work_order/<br />
**METHOD**:GET <br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| user_id | int | Y | 用户id|

**RESPONSE**:


####5.查看一条工单详情(如果是已处理的，将查看状态改为已查看)

**URI**: /rpc/work_order/<data_id>/<br />
**METHOD**:GET <br />
**PARAMS**:

**RESPONSE**:


####6.评价一条工单

**URI**: /rpc/work_order/<data_id>/<br />
**METHOD**:PUT <br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| commnet | int | Y | 1=满意 2=不满意|

**RESPONSE**:



####7. 申请加急

**URI**: /rpc/work_order/urgent/<data_id>/<br />
**METHOD**:PUT <br />
**PARAMS**:

**RESPONSE**:



####8. 获取已处理未查看的订单数量

**URI**: /rpc/get_unchecked_count/<br />
**METHOD**:GET <br />
**PARAMS**:

**RESPONSE**:



####9. 获取支付订单列表/获取充值订单列表

**URI**: /rpc/get_recent_orders/<br />
**METHOD**:GET <br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| pro_id | int | Y | 平台id（凯撒=1，奥丁=2，满贯=3）|
| player_id | int | Y | 用户ID |
| type_id | int | Y | 充值=1， 提现=2 |

**RESPONSE**:










## 提交工单

####1. 充值申诉

**URI**: /rpc/work_order/<br />
**METHOD**:POST <br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| pro_id | int | Y | 平台id（凯撒=1，奥丁=2，满贯=3） |
| type_id | int | Y | 1=充值问题 |
| player_id | int | Y | 用户ID |
| pay_id | int | Y | 充值订单号 |
| pay_type | int | Y | 支付类型 |
| amount | float | Y | 金额 |
| pay_at | datatime | Y | 充值时间 |
| pay_name | string | Y | 付款实名制姓名 |
| images | list | Y | 图片 |

| chn | string | Y | 当前渠道 |
| game_version | string | Y | 游戏版本 |
| ua | string | Y | 机型 |
| platform | string | Y | 手机系统 |
| level | int | Y | 用户等级 |

**RESPONSE**:


####2. 提现反馈

**URI**: /rpc/work_order/<br />
**METHOD**:POST <br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| pro_id | int | Y | 平台id（凯撒=1，奥丁=2，满贯=3） |
| type_id | int | Y | 2=提现问题 |
| player_id | int | Y | 用户ID |
| withdraw_id | int | Y | 提现订单号 |
| amount | float | Y | 金额 |
| withdraw_at | datatime | Y | 提现时间 |
| question_type | int | Y | 问题分类 201=提现未到账 202=其他问题|
|images | list | Y | 图片 |

| chn | string | Y | 当前渠道 |
| game_version | string | Y | 游戏版本 |
| ua | string | Y | 机型 |
| platform | string | Y | 手机系统 |
| level | int | Y | 用户等级 |

**RESPONSE**:


####3. 账号反馈

**URI**: /rpc/work_order/<br />
**METHOD**:POST <br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| pro_id | int | Y | 平台id（凯撒=1，奥丁=2，满贯=3） |
| type_id | int | Y | 3=账号问题 |
| player_id | int | Y | 用户ID |
| appear_at | datatime | Y | 出现时间 |
| question_type | int | Y | 问题分类  301=无法注册 302=异常登录 303=保险柜问题 304=无法接收验证码 305=其他问题|
| desc | string | Y | 描述 |
|images | list | Y | 图片 |

| chn | string | Y | 当前渠道 |
| game_version | string | Y | 游戏版本 |
| ua | string | Y | 机型 |
| platform | string | Y | 手机系统 |
| level | int | Y | 用户等级 |

**RESPONSE**:


####4. 游戏反馈

**URI**: /rpc/work_order/<br />
**METHOD**:POST <br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| pro_id | int | Y | 平台id（凯撒=1，奥丁=2，满贯=3） |
| type_id | int | Y | 4=游戏问题 |
| player_id | int | Y | 用户ID |
| appear_at | datatime | Y | 出现时间 |
| question_type | int | Y | 问题分类 401=掉线 402=闪退 403=自动弃牌 404=结算未派奖 405=点击操作按钮无反应 406=游戏卡住无法退出 407=其他问题|
| game | int | Y | 游戏 |
| desc | string | Y | 描述 |
|images | list | Y | 图片 |

| chn | string | Y | 当前渠道 |
| game_version | string | Y | 游戏版本 |
| ua | string | Y | 机型 |
| platform | string | Y | 手机系统 |
| level | int | Y | 用户等级 |

**RESPONSE**:


####5. 商务合作

**URI**: /rpc/work_order/<br />
**METHOD**:POST <br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| pro_id | int | Y | 平台id（凯撒=1，奥丁=2，满贯=3） |
| type_id | int | Y | 5=商务合作 |
| player_id | int | Y | 用户ID |
| cooperation | string | Y | 合作信息 |
| chn | string | Y | 当前渠道 |
| game_version | string | Y | 游戏版本 |
| ua | string | Y | 机型 |
| platform | string | Y | 手机系统 |
| level | int | Y | 用户等级 |


```javascript
    cooperation
    {
        'contact_type': 3,                     联系方式 1=微信 2=QQ 3=手机号
        'contact_account': '13012333221',      联系账号
        'source_desc': 'adsadad',              资源
        'is_veteran': 1,                       是否有经验 1=是 2=否
        'with_lower' 1,                        是否有下级团队 1=是 2=否
        'lower_count': 0=无 1=0-50 2=51-200 3=201-500 4=500+  团队人数
        'daily_new': 1=0-50 2=51-200 3=210-500 4=501-1000 5=1000+  每日带来新用户
    }|
```

**RESPONSE**:


####6. 其他问题

**URI**: /rpc/work_order/<br />
**METHOD**:POST <br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| pro_id | int | Y | 平台id（凯撒=1，奥丁=2，满贯=3） |
| type_id | int | Y | 6=其他问题 |
| player_id | int | Y | 用户ID |
| appear_at | datatime | Y | 出现时间 |
| desc | string | Y | 描述 |
| images | list | Y | 图片 |
| question_type | int | Y | 问题分类 601=问题 602=建议|

| chn | string | Y | 当前渠道 |
| game_version | string | Y | 游戏版本 |
| ua | string | Y | 机型 |
| platform | string | Y | 手机系统 |
| level | int | Y | 用户等级 |

**RESPONSE**:


## 登陆申诉

####1. 登陆申诉

**URI**: /rpc/login_order/<br />
**METHOD**:POST <br />
**PARAMS**:

| name | type | required | description |
| -------- | -------- |-------- | -------- |
| pro_id | int | Y | 平台id（凯撒=1，奥丁=2，满贯=3） |
| appear_at | datatime | Y | 出现时间 |
| net | int | Y | 网络 0=Wi-Fi 1=4G 2=3G 3=2G|
| contact_type | int | Y | 联系方式 1=手机 2=微信 3=QQ |
| contact_account | string | N | 联系账号|
| phone_model | string | Y | 手机型号|
| desc | string | N | 问题描述|
| images | list | N | 图片 |

**RESPONSE**:


####2. 图片上传相关接口

rpc/login_order/get_upload_token/

rpc/login_order/get_file_path/




