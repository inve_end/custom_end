from django.db import models
from gis.common.django_ext.models import BaseModel, ListField
from gis.admin.models import User, ProConfig


class ProblemType(BaseModel):
    desc = models.CharField(max_length=100)
    image = models.CharField(max_length=100)
    button = models.CharField(max_length=100)

    class Meta:
        db_table = 'problem_type'


class CommonProblem(BaseModel):
    type = models.ForeignKey(ProblemType, on_delete=models.CASCADE, db_constraint=False)
    title = models.CharField(max_length=200)
    answer = models.CharField(max_length=1000)
    pro = models.ForeignKey(ProConfig, on_delete=models.CASCADE, db_constraint=False)

    class Meta:
        db_table = 'common_problem'


class Phrase(BaseModel):
    type = models.ForeignKey(ProblemType, on_delete=models.CASCADE, db_constraint=False)
    temp_type = models.SmallIntegerField()
    title = models.CharField(max_length=200, null=True)
    context = models.CharField(max_length=1000)
    pro = models.ForeignKey(ProConfig, on_delete=models.CASCADE, db_constraint=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, db_constraint=False, null=True)

    class Meta:
        db_table = 'phrase'


class AnnounceMent(BaseModel):
    start_at = models.DateTimeField()
    end_at = models.DateTimeField()
    pkg = models.CharField(max_length=500)
    chn = models.CharField(max_length=500)
    system = models.CharField(max_length=200)
    title = models.CharField(max_length=200)
    context = models.CharField(max_length=1000)
    pro = models.ForeignKey(ProConfig, on_delete=models.CASCADE, db_constraint=False)

    class Meta:
        db_table = 'announcement'


class WorkOrder(BaseModel):
    status = models.SmallIntegerField(default=0)  # 状态 0=未处理 1=已处理
    finish_at = models.DateTimeField(null=True)  # 工单处理时间
    is_solved = models.SmallIntegerField(null=True)  # 问题是否解决 0=未解决 1=已解决
    handler = models.ForeignKey(User, on_delete=models.CASCADE, db_constraint=False, null=True)  # 当前操作人
    # reassign_status = models.SmallIntegerField(null=True)  # 转接状态 0=未转接 1=已转接
    reassign_at = models.DateTimeField(null=True)  # 转接时间
    urgent_count = models.SmallIntegerField(default=0)  # 加急次数
    urgent_at = models.DateTimeField(null=True)  # 上次加急时间
    process_time = models.IntegerField(null=True)  # 处理时长
    pro = models.ForeignKey(ProConfig, on_delete=models.CASCADE, db_constraint=False)  # 平台
    type = models.ForeignKey(ProblemType, on_delete=models.CASCADE, db_constraint=False)  # 问题类型
    player_id = models.BigIntegerField()  # 用户ID
    level = models.SmallIntegerField()   # 用户等级
    chn = models.CharField(max_length=200)  # 用户渠道
    game_version = models.CharField(max_length=200)  # 游戏版本号
    ua = models.CharField(max_length=200)  # 用户机型
    platform = models.CharField(max_length=200)  # 收集系统 ios/andoird
    game = models.SmallIntegerField(null=True)  # 游戏
    question_type = models.SmallIntegerField(null=True)  # 问题分类
    desc = models.CharField(max_length=1000, null=True)  # 问题描述
    appear_at = models.DateTimeField(null=True)  # 出现时间
    images = ListField(max_length=1000, null=True)  # 上传图片
    withdraw_id = models.BigIntegerField(null=True)  # 提现ID
    withdraw_at = models.DateTimeField(null=True)  # 提现时间
    amount = models.FloatField(null=True)  # 提现金额/充值金额
    appeal_type = models.SmallIntegerField(null=True)  # 申诉订单类型 1=JP 2=UA 3=MP
    company = models.CharField(max_length=100, null=True)  # 公司名称
    pay_id = models.BigIntegerField(null=True)  # 充值订单ID
    pay_type = models.SmallIntegerField(null=True)  # 充值类型
    pay_name = models.CharField(max_length=50, null=True)  # 付款名称/收款卡姓名
    pay_at = models.DateTimeField(null=True)  # 订单创建时间
    remark = models.CharField(max_length=500, null=True)  # 备注
    response = models.CharField(max_length=1000, null=True)  # 回复内容
    cooperation = models.TextField(null=True)  # 合作详情
    comment = models.SmallIntegerField(null=True)  # 用户评价 1=满意 2=不满意
    extend = models.TextField(null=True)
    checked = models.SmallIntegerField(null=True)   # 是否查看 0=未查看 1=已查看

    class Meta:
        db_table = 'work_order'


class Reassign(BaseModel):
    order = models.ForeignKey(WorkOrder, on_delete=models.CASCADE, db_constraint=False)
    player_id = models.BigIntegerField()
    from_user = models.ForeignKey(User, related_name='from_user', on_delete=models.CASCADE, db_constraint=False)
    to_user = models.ForeignKey(User, related_name='to_user', on_delete=models.CASCADE, db_constraint=False)
    operator = models.ForeignKey(User, related_name='operator', on_delete=models.CASCADE, db_constraint=False)
    stay_time = models.IntegerField()
    pro = models.ForeignKey(ProConfig, on_delete=models.CASCADE, db_constraint=False)

    class Meta:
        db_table = 'reassign'


class Performance(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, db_constraint=False)
    day = models.DateField()
    type = models.ForeignKey(ProblemType, on_delete=models.CASCADE, db_constraint=False)
    finish_count = models.IntegerField(default=0)
    wait_count = models.IntegerField(default=0)
    comment_good_count = models.IntegerField(default=0)
    comment_bad_count = models.IntegerField(default=0)
    good_percent = models.FloatField(default=0)
    avg_process_time = models.FloatField(default=0)

    class Meta:
        db_table = 'performance'


class Report(BaseModel):
    pro_id = models.SmallIntegerField()
    day = models.DateField()
    type = models.ForeignKey(ProblemType, on_delete=models.CASCADE, db_constraint=False)
    second_type = models.IntegerField(null=True)
    third_type = models.IntegerField(null=True)
    total_count = models.IntegerField(default=0)
    finish_count = models.IntegerField(default=0)
    wait_count = models.IntegerField(default=0)
    solved_count = models.IntegerField(default=0)
    unsolved_count = models.IntegerField(default=0)

    class Meta:
        db_table = 'report'


class LoginOrder(BaseModel):
    pro_id = models.SmallIntegerField()
    handler = models.ForeignKey(User, on_delete=models.CASCADE, db_constraint=False, null=True)  # 处理人
    status = models.SmallIntegerField(default=0)  # 状态 0=未处理 1=处理中 2=已处理
    appear_at = models.DateTimeField(null=True)
    net = models.SmallIntegerField()
    phone_model = models.CharField(max_length=200, null=True)
    contact_type = models.SmallIntegerField(null=True)
    contact_account = models.CharField(max_length=200, null=True)
    desc = models.CharField(max_length=1000, null=True)
    images = ListField(max_length=1000, null=True)  # 上传图片
    remark = models.TextField(null=True)
    extend = models.TextField(null=True)

    class Meta:
        db_table = 'login_order'
