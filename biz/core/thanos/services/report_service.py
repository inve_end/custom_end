"""
报表管理
"""
import logging

from django.db.models import Sum

from biz.core.thanos.models import Report
from biz.core.thanos.services import problem_type_service
from biz.const import SecondType, ThirdType
from biz.utils.export_file import gen_filename, redirect_to_file
from gis.common.django_ext.models import paginate

_LOGGER = logging.getLogger(__name__)


def list_report(pro_id, page_no, page_size, type_id=None, second_type=None, third_type=None, day_start=None,
                day_end=None):
    assert page_no > 0
    assert page_size > 0
    assert pro_id
    query = Report.objects.filter(pro_id=pro_id).all()
    if type_id:
        query = query.filter(type_id=type_id)
    if second_type:
        query = query.filter(second_type=second_type)
    if third_type:
        query = query.filter(third_type=third_type)
    if day_start:
        query = query.filter(day__gte=day_start)
    if day_end:
        query = query.filter(day__lte=day_end)
    return query.count(), [e.to_dict() for e in paginate(query, page_no, page_size)]


def get_ttl_data(pro_id, type_id=None, second_type=None, third_type=None, day_start=None, day_end=None):
    assert pro_id
    query = Report.objects.filter(pro_id=pro_id).all()
    if type_id:
        query = query.filter(type_id=type_id)
    if second_type:
        query = query.filter(second_type=second_type)
    if third_type:
        query = query.filter(third_type=third_type)
    if day_start:
        query = query.filter(day__gte=day_start)
    if day_end:
        query = query.filter(day__lte=day_end)
    ttl_data = query.aggregate(Sum('finish_count'), Sum('wait_count'), Sum('solved_count'),
                               Sum('unsolved_count'))
    ttl_finish_count = ttl_data['finish_count__sum'] if ttl_data.get('finish_count__sum') else 0
    ttl_wait_count = ttl_data['wait_count__sum'] if ttl_data.get('wait_count__sum') else 0
    ttl_solved_count = ttl_data['solved_count__sum'] if ttl_data.get('solved_count__sum') else 0
    ttl_unsolved_count = ttl_data['unsolved_count__sum'] if ttl_data.get('unsolved_count__sum') else 0
    ttl_count = ttl_finish_count + ttl_wait_count
    return {
        'ttl_finish_count': ttl_finish_count,
        'ttl_wait_count': ttl_wait_count,
        'ttl_count': ttl_count,
        'ttl_solved_count': ttl_solved_count,
        'ttl_unsolved_count': ttl_unsolved_count,
    }


def download(pro_id, type_id=None, second_type=None, third_type=None, day_start=None, day_end=None):
    cn_header = ['日期', '问题类型', '二级分类', '三级分类', '反馈次数', '已处理数量', '待处理数量', '已解决数量', '未解决数量']
    filename = gen_filename('report')
    data = _export_data(pro_id, type_id, second_type, third_type, day_start, day_end)
    return redirect_to_file(data, cn_header, filename)


def _export_data(pro_id, type_id=None, second_type=None, third_type=None, day_start=None, day_end=None):
    assert pro_id
    query = Report.objects.filter(pro_id=pro_id).all()
    if type_id:
        query = query.filter(type_id=type_id)
    if second_type:
        query = query.filter(second_type=second_type)
    if third_type:
        query = query.filter(third_type=third_type)
    if day_start:
        query = query.filter(day__gte=day_start)
    if day_end:
        query = query.filter(day__lte=day_end)
    data = []
    problem_desc_dict = {}
    for item in query:
        if problem_desc_dict.get(item.type_id):
            problem_desc = problem_desc_dict[item.type_id]
        else:
            _problem = problem_type_service.get_problem_type(item.type_id)
            problem_desc = _problem['desc']
            problem_desc_dict[item.type_id] = problem_desc
        data.append((
            item.day,
            problem_desc,
            SecondType[item.second_type] if item.second_type else '',
            ThirdType[item.third_type] if item.third_type else '',
            item.total_count,
            item.finish_count,
            item.wait_count,
            item.solved_count,
            item.unsolved_count
        ))
    return data
