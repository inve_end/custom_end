"""
问题类型配置
"""
import logging

from biz.core.thanos.models import ProblemType
from biz.exceptions import ERROR_DATA_NOT_EXISTS
from gis.common.django_ext.models import paginate
from gis.common.exceptions import BizException

_LOGGER = logging.getLogger(__name__)


def list_problem_type(page_no, page_size):
    assert page_no > 0
    assert page_size > 0
    query = ProblemType.objects.all()
    return query.count(), [e.to_dict() for e in paginate(query, page_no, page_size)]


def get_problem_type(data_id):
    data = _get_data(data_id)
    return data.to_dict()


def update_problem_type(data_id, desc, image, button):
    data = _get_data(data_id)
    data.desc = desc
    data.image = image
    data.button = button
    data.save()
    return data.to_dict()


def _get_data(data_id):
    assert data_id > 0
    data = ProblemType.objects.filter(pk=data_id).first()
    if not data:
        raise BizException(ERROR_DATA_NOT_EXISTS)
    return data
