"""
远程获取产品数据
"""
import logging
from requests import RequestException

from gis.common.retry import retry
from gis.admin.services import admin_service
from biz.utils import rpc


_LOGGER = logging.getLogger(__name__)


@retry(RequestException)
def get_recent_order(pro_id, player_id, type_id):
    pro = admin_service.get_pro_config(pro_id)
    api_key = pro['api_key']
    data_url = pro['get_data_url']
    params = {
        'user_id': player_id,
        'question_type': type_id,
    }
    resp = rpc.do_post(data_url, params, api_key)
    if resp['status'] == 0:
        _LOGGER.info(resp['data']['list'])
        return resp['data']['list'] or []






