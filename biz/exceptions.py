from gis.common.exceptions import BizErrorCode

ERROR_DATA_NOT_EXISTS = BizErrorCode(101, '数据不存在')


ERROR_KEFU_UPDATE_PUBLIC_PHRASE = BizErrorCode(1001, '客服不能操作公共模版')
ERROR_NO_PERMISSION_OPERATION = BizErrorCode(1002, '无权操作该数据')
ERROR_DATA_STATUS = BizErrorCode(1002, '数据状态错误')

ERROR_TIME_LIMIT = BizErrorCode(2001, '5分钟内不能重复提交')





