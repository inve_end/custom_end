from enum import IntEnum, unique, Enum

from django.conf import settings


@unique
class PayType(IntEnum):
    """
    支付类型
    """
    CLOUD_FLASH = 88
    JUSTPAY_WX = 1000
    JUSTPAY_ALI = 1001
    JUSTPAY_UNION = 1002
    JUSTPAY_QQ = 1012
    JUSTPAY_JD = 1013
    AGENCY_CHARGE = 1003
    AGENCY_APPLY = 1004
    APPLE_IAP = 1005
    CARD_PAY = 1019
    JUSTPAY_TEST = 1111
    UNION_TRANSFER = 1201  # UA
    QUOTA_ALIPAY = 1202
    QUOTA_WXPAY = 1203
    JUSTPAY_WXAPP = 1204
    OFFICE_AGENCY_CHARGE = 1205  # UA
    QR_ALIPAY = 1206
    JUSTPAY_HYDRA = 1207
    QUOTA_HYDRA = 1208
    MF_PAY = 4888  # 秒付


@unique
class AppealType(IntEnum):
    """
    充值申诉分类
    """
    JUSTPAY = 1
    UNIONAGENCY = 2
    MIAOPAY = 3


@unique
class ProblemType(IntEnum):
    """
    工单状态
    """
    RECHARGE = 1
    WITHDRAW = 2
    ACCOUNT = 3
    GAME = 4
    COOPERATION = 5
    OTHER = 6


@unique
class Role(IntEnum):
    """
    角色
    """
    KEFU = 1
    KEFU_ADMIN = 2
    YUNYIN = 3
    CHARGE_YUNYIN = 4


@unique
class TempType(IntEnum):
    """
    模版类型
    """
    PRIVATE = 1
    PUBLIC = 2


@unique
class WorkOrderStatus(IntEnum):
    """
    工单状态
    """
    WAITING = 0
    FINISHED = 1


@unique
class LoginOrderStatus(IntEnum):
    """
    工单状态
    """
    WAITING = 0
    WORKING = 1
    FINISHED = 2


@unique
class UserStatus(IntEnum):
    """
    客服工作状态
    """
    REST = 0
    WORKING = 1


@unique
class CheckStatus(IntEnum):
    """
    工单查看
    """
    WAIT = 0
    CHECKED = 1


@unique
class Comment(IntEnum):
    """
    工单评价
    """
    GOOD = 1
    BAD = 2


@unique
class IsSolved(IntEnum):
    """
    是否解决
    """
    UNSOLVED = 0
    SOLVED = 1


SecondType = {
    201: '提现未到账',
    202: '其他问题',
    301: '无法注册',
    302: '异常登录',
    303: '保险柜问题',
    304: '无法接收验证码',
    305: '其他问题',
    401: '掉线',
    402: '闪退',
    403: '自动弃牌',
    404: '结算未派奖',
    405: '点击操作按钮无反应',
    406: '游戏卡住无法退出',
    407: '其他问题',
    601: '问题',
    602: '建议',
}

ThirdType = {
    1: '捕鱼',
    2: '斗地主',
    3: '龙虎斗',
    4: '水浒传',
    5: '炸金花',
    6: '金鲨银鲨',
    7: '血拼牛牛',
    8: '百人牛牛',
    9: '红黑大战',
    10: '抢庄牛牛',
}