from django.urls import path, include

from biz.views import rpc_views, utils_views, problem_type_views, common_problem_views, announcement_views, \
    phrase_views, work_order_views, performance_views, admin_views, report_views, login_order_views

urlpatterns = [
    path('admin/', include('gis.admin.urls')),

    path('rpc/get_token/', rpc_views.GetTokenView.as_view()),

    # h5调用的接口
    path('rpc/problem_type/', rpc_views.ProblemTypeListView.as_view()),  # 获取问题类型
    path('rpc/common_problem/', rpc_views.CommonProblemListView.as_view()),  # 获取常见问题问题
    path('rpc/announcement/', rpc_views.AnnouncementListView.as_view()),  # 获取公告

    path('rpc/get_upload_token/', rpc_views.UploadTokenView.as_view()),
    path('rpc/get_file_path/', rpc_views.GetFilePathView.as_view()),

    path('rpc/work_order/', rpc_views.WorkOrderListView.as_view()),  # 用户提交一个工单 ， 用户获取工单列表
    path('rpc/get_unchecked_count/', rpc_views.UnCheckedCountView.as_view()),  # 已处理未查看的订单数量
    path('rpc/work_order/<int:data_id>/', rpc_views.WorkOrderDeatilView.as_view()),  # 用户查看一个工单， 评价
    path('rpc/work_order/urgent/<int:data_id>/', rpc_views.WorkOrderUrgentView.as_view()),  # 申请加急

    path('rpc/get_recent_orders/', rpc_views.GetRecentOrderView.as_view()),  # 获取 支付订单列表/提现订单列表

    path('rpc/login_order/get_upload_token/', rpc_views.LoginOrderUploadTokenView.as_view()),
    path('rpc/login_order/get_file_path/', rpc_views.LoginOrderGetFilePathView.as_view()),
    path('rpc/login_order/', rpc_views.LoginOrderListView.as_view()),  # 提交登陆申诉


    # 问题类型配置
    path('api/get_upload_token/', utils_views.UploadTokenView.as_view()),
    path('api/get_file_path/', utils_views.GetFilePathView.as_view()),
    path('api/problem_type/', problem_type_views.ProblemTypeListView.as_view()),
    path('api/problem_type/<int:data_id>/', problem_type_views.ProblemTypeDetailView.as_view()),

    # 常见问题配置
    path('api/common_problem/', common_problem_views.CommonProblemListView.as_view()),
    path('api/common_problem/<int:data_id>/', common_problem_views.CommonProblemDetailView.as_view()),

    # 客服公告配置
    path('api/announcement/', announcement_views.AnnouncementListView.as_view()),
    path('api/announcement/<int:data_id>/', announcement_views.AnnouncementDetailView.as_view()),

    # 客服常用语配置
    path('api/phrase/', phrase_views.PhraseListView.as_view()),
    path('api/phrase/<int:data_id>/', phrase_views.PhraseDetailView.as_view()),

    # 登陆申诉
    path('api/login_order/', login_order_views.LoginOrderListView.as_view()),
    # 充值申诉
    path('api/appeal/', work_order_views.AppealListView.as_view()),
    # 客服回单
    path('api/customer_order/', work_order_views.CustomerOrderListView.as_view()),
    path('api/customer/phrase/', phrase_views.CustomerPhraseView.as_view()),
    # 工单管理
    path('api/work_order/', work_order_views.WorkOrderListView.as_view()),
    # 工单，充值申诉 查看和处理 都调用这个接口
    path('api/work_order/<int:data_id>/', work_order_views.WorkOrderDetailView.as_view()),
    # 转接时获取在线客服
    path('api/work_order/get_online_user/', work_order_views.GetOnlineUserView.as_view()),
    # 转接操作的接口
    path('api/work_order/reassign/<int:data_id>/', work_order_views.WorkOrderReassignView.as_view()),
    # 获取转接记录列表
    path('api/reassign/', work_order_views.ReassignListView.as_view()),

    # 绩效管理
    path('api/performance/', performance_views.PerformanceListView.as_view()),
    path('api/performance/ttl/', performance_views.PerformanceTtlView.as_view()),
    path('api/performance/download/', performance_views.DownLoadView.as_view()),

    # 报表管理
    path('api/report/', report_views.ReportListView.as_view()),
    path('api/report/ttl/', report_views.ReportTtlView.as_view()),
    path('api/report/download/', report_views.DownLoadView.as_view()),

    # 获取客服/客服主管/客服和支付运营
    path('api/get_kefu/', admin_views.GetKefuView.as_view()),
    path('api/get_kefu_admin/', admin_views.GetKefuAdminView.as_view()),
    path('api/get_report_admin/', admin_views.GetReportAdminView.as_view()),

]
