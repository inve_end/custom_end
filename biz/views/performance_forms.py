from marshmallow import fields

from gis.common.django_ext.forms import PaginationSchema, BaseSchema


class PerformanceListSchema(PaginationSchema):
    user_id = fields.Integer()
    type_id = fields.Integer()
    day_start = fields.Date()
    day_end = fields.Date()


class PerformanceTtlSchema(BaseSchema):
    user_id = fields.Integer()
    type_id = fields.Integer()
    day_start = fields.Date()
    day_end = fields.Date()
