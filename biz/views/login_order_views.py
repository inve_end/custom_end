from django.utils.decorators import method_decorator

from biz.core.thanos.services import login_order_service
from biz.views.login_order_forms import *
from .helper import wapper_login_order
from gis.admin.decorators import require_login, check_permission
from gis.common.django_ext.decorators import validate_parameters
from gis.common.django_ext.views import BaseView, PaginationResponse


class LoginOrderListView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('login_order_get'))
    @method_decorator(validate_parameters(ListLoginOrderSchema))
    def get(self, request, cleaned_data):
        assert request.pro_id
        total, ls = login_order_service.list_login_order(pro_id=request.pro_id, **cleaned_data)
        return PaginationResponse(total, [wapper_login_order(e) for e in ls])


class LoginOrderDetailView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('login_order_get'))
    def get(self, request, data_id):
        data = login_order_service.get_login_order(data_id)
        return wapper_login_order(data)

    @method_decorator(require_login)
    @method_decorator(check_permission('login_order_mod'))
    @method_decorator(validate_parameters(UpdateLoginOrderSchema))
    def put(self, request, data_id, cleaned_data):
        data = login_order_service.update_login_order(data_id=data_id, handler_id=request.user_id, **cleaned_data)
        return data





