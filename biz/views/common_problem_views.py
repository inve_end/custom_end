from django.utils.decorators import method_decorator

from biz.core.thanos.services import common_problem_service
from biz.views.common_problem_forms import *
from gis.admin.decorators import require_login, check_permission
from gis.common.django_ext.decorators import validate_parameters
from gis.common.django_ext.views import BaseView, PaginationResponse
from .helper import *


class CommonProblemListView(BaseView):

    @method_decorator(require_login)
    @method_decorator(check_permission('common_problem_get'))
    @method_decorator(validate_parameters(ListCommonProblemSchema))
    def get(self, request, cleaned_data):
        assert request.pro_id
        total, ls = common_problem_service.list_common_problem(pro_id=request.pro_id, **cleaned_data)
        return PaginationResponse(total, [wapper_problem_type(e) for e in ls])

    @method_decorator(require_login)
    @method_decorator(check_permission('common_problem_add'))
    @method_decorator(validate_parameters(AddCommonProblemSchema))
    def post(self, request, cleaned_data):
        assert request.pro_id
        return common_problem_service.add_common_problem(pro_id=request.pro_id, **cleaned_data)


class CommonProblemDetailView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('common_problem_get'))
    def get(self, request, data_id):
        data = common_problem_service.get_common_problem(data_id)
        return data

    @method_decorator(require_login)
    @method_decorator(check_permission('common_problem_mod'))
    @method_decorator(validate_parameters(UpdateCommonProblemSchema))
    def put(self, request, data_id, cleaned_data):
        data = common_problem_service.update_common_problem(data_id=data_id, **cleaned_data)
        return data

    @method_decorator(require_login)
    @method_decorator(check_permission('common_problem_del'))
    def delete(self, request, data_id):
        common_problem_service.delete_common_problem(data_id)
        return dict(id=data_id)

