from marshmallow import fields

from gis.common.django_ext.forms import PaginationSchema, BaseSchema


class ReportListSchema(PaginationSchema):
    type_id = fields.Integer()
    second_type = fields.Integer()
    third_type = fields.Integer()
    day_start = fields.Date()
    day_end = fields.Date()


class ReportTtlSchema(BaseSchema):
    type_id = fields.Integer()
    second_type = fields.Integer()
    third_type = fields.Integer()
    day_start = fields.Date()
    day_end = fields.Date()
