from marshmallow import fields

from gis.common.django_ext.forms import BaseSchema, PaginationSchema


class ListWorkOrderSchema(PaginationSchema):
    type_id = fields.Integer()
    player_id = fields.Integer()
    data_id = fields.Integer()
    created_at_start = fields.DateTime()
    created_at_end = fields.DateTime()
    status = fields.Integer()
    level = fields.Integer()
    is_solved = fields.Integer()
    handler_id = fields.Integer()


class UpdateWorkOrderSchema(BaseSchema):
    response = fields.String()
    is_solved = fields.Integer()
    status = fields.Integer()
    remark = fields.String()


class ReassginWorkOrderSchema(BaseSchema):
    to_user_id = fields.Integer(required=True)


class ListReassignSchema(PaginationSchema):
    order_id = fields.Integer()
    player_id = fields.Integer()
    from_user_id = fields.Integer()
    to_user_id = fields.Integer()
    operator_id = fields.Integer()
    created_at_start = fields.DateTime()
    created_at_end = fields.DateTime()


class ListCustomerOrderSchema(PaginationSchema):
    data_id = fields.Integer()
    player_id = fields.Integer()
    type_id = fields.Integer()
    level = fields.Integer()
    status = fields.Integer()
    created_at_start = fields.DateTime()
    created_at_end = fields.DateTime()


class ListAppealSchema(PaginationSchema):
    data_id = fields.Integer()
    pay_id = fields.Integer()
    player_id = fields.Integer()
    pay_type = fields.Integer()
    appeal_type = fields.Integer()
    status = fields.Integer()
    pay_name = fields.String()
    amount_start = fields.Float()
    amount_end = fields.Float()
    created_at_start = fields.DateTime()
    created_at_end = fields.DateTime()
    pay_at_start = fields.DateTime()
    pay_at_end = fields.DateTime()


class GetOnlineUserSchema(PaginationSchema):
    type_id = fields.Integer(required=True)
    handler_id = fields.Integer(required=True)
