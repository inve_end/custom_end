from django.utils.decorators import method_decorator

from biz.core.thanos.services import phrase_service
from biz.views.phrase_forms import *
from .helper import wapper_problem_type
from gis.admin.decorators import require_login, check_permission
from gis.common.django_ext.decorators import validate_parameters
from gis.common.django_ext.views import BaseView, PaginationResponse


class PhraseListView(BaseView):

    @method_decorator(require_login)
    @method_decorator(check_permission('phrase_get'))
    @method_decorator(validate_parameters(ListPhraseSchema))
    def get(self, request, cleaned_data):
        total, ls = phrase_service.list_phrase(user=request.user, **cleaned_data)
        return PaginationResponse(total, [wapper_problem_type(e) for e in ls])

    @method_decorator(require_login)
    @method_decorator(check_permission('phrase_add'))
    @method_decorator(validate_parameters(AddPhraseSchema))
    def post(self, request, cleaned_data):
        return phrase_service.add_phrase(user=request.user, **cleaned_data)


class PhraseDetailView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('phrase_get'))
    def get(self, request, data_id):
        data = phrase_service.get_phrase(data_id)
        return data

    @method_decorator(require_login)
    @method_decorator(check_permission('phrase_mod'))
    @method_decorator(validate_parameters(UpdatePhraseSchema))
    def put(self, request, data_id, cleaned_data):
        data = phrase_service.update_phrase(data_id=data_id, user=request.user, **cleaned_data)
        return data

    @method_decorator(require_login)
    @method_decorator(check_permission('phrase_del'))
    def delete(self, request, data_id):
        phrase_service.delete_phrase(data_id, request.user)
        return dict(id=data_id)


class CustomerPhraseView(BaseView):

    @method_decorator(require_login)
    @method_decorator(check_permission('phrase_get'))
    @method_decorator(validate_parameters(CustomerPhraseSchema))
    def get(self, request, cleaned_data):
        ls1, ls2 = phrase_service.list_customer_phrase(user=request.user, **cleaned_data)
        return dict(pub=ls1, pri=ls2)
