from django.utils.decorators import method_decorator

from biz.core.thanos.services import problem_type_service
from biz.views.problem_type_forms import *
from .helper import wapper_image_path
from gis.admin.decorators import require_login, check_permission
from gis.common.django_ext.decorators import validate_parameters
from gis.common.django_ext.views import BaseView, PaginationResponse


class ProblemTypeListView(BaseView):

    @method_decorator(require_login)
    # 因为很多功能的筛选条件都需要问题类型数据，故屏蔽这个接口的校验
    # @method_decorator(check_permission('problem_type_get'))
    @method_decorator(validate_parameters(PaginationSchema))
    def get(self, request, cleaned_data):
        total, ls = problem_type_service.list_problem_type(**cleaned_data)
        return PaginationResponse(total, [wapper_image_path(e) for e in ls])


class ProblemTypeDetailView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('problem_type_get'))
    def get(self, request, data_id):
        data = problem_type_service.get_problem_type(data_id)
        return wapper_image_path(data)

    @method_decorator(require_login)
    @method_decorator(check_permission('problem_type_mod'))
    @method_decorator(validate_parameters(UpdateProblemTypeSchema))
    def put(self, request, data_id, cleaned_data):
        data = problem_type_service.update_problem_type(data_id=data_id, **cleaned_data)
        return data

