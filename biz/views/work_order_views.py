from django.utils.decorators import method_decorator

from biz.core.thanos.services import work_order_service
from biz.views.work_order_forms import *
from biz.const import UserStatus, Role, ProblemType
from .helper import wapper_work_order, wapper_reassign, wapper_image_path, wapper_user_data
from gis.admin.decorators import require_login, check_permission
from gis.admin.services import admin_service
from gis.common.django_ext.decorators import validate_parameters
from gis.common.django_ext.views import BaseView, PaginationResponse


class WorkOrderListView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('work_order_get'))
    @method_decorator(validate_parameters(ListWorkOrderSchema))
    def get(self, request, cleaned_data):
        assert request.pro_id
        total, ls = work_order_service.list_work_order(pro_id=request.pro_id, order_by_urgent=True, **cleaned_data)
        return PaginationResponse(total, [wapper_work_order(e) for e in ls])


class WorkOrderDetailView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission(['work_order_get', 'appeal_get', 'customer_order_get']))
    def get(self, request, data_id):
        data = work_order_service.get_work_order(data_id)
        return wapper_work_order(data)

    @method_decorator(require_login)
    @method_decorator(check_permission(['appeal_mod', 'customer_order_mod']))
    @method_decorator(validate_parameters(UpdateWorkOrderSchema))
    def put(self, request, data_id, cleaned_data):
        data = work_order_service.update_work_order(data_id=data_id, handler_id=request.user_id, **cleaned_data)
        return data


# 转接时，获取在线客服
class GetOnlineUserView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('work_order_reassign'))
    @method_decorator(validate_parameters(GetOnlineUserSchema))
    def get(self, request, cleaned_data):
        assert request.pro_id
        total, ls = admin_service.list_users(pro_id=request.pro_id, status=UserStatus.WORKING, role_id=Role.KEFU,
                                             page_no=1, page_size=100)
        """
        获取ls用户 待处理订单量，昨日满意度
        """
        # 不展示当前工单所属客服
        for i in ls:
            if i['id'] == cleaned_data['handler_id']:
                ls.remove(i)
                total -= 1
        ls = [wapper_user_data(e, cleaned_data['type_id']) for e in ls]

        return PaginationResponse(total, ls)


# 转接
class WorkOrderReassignView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('work_order_reassign'))
    @method_decorator(validate_parameters(ReassginWorkOrderSchema))
    def put(self, request, data_id, cleaned_data):
        data = work_order_service.reassign_work_order(data_id=data_id, user_id=request.user_id, **cleaned_data)
        return data


# 获取转接列表
class ReassignListView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('reassign_get'))
    @method_decorator(validate_parameters(ListReassignSchema))
    def get(self, request, cleaned_data):
        assert request.pro_id
        total, ls = work_order_service.list_reassign(pro_id=request.pro_id, **cleaned_data)
        return PaginationResponse(total, [wapper_reassign(e) for e in ls])


# 获取客服回单
class CustomerOrderListView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('customer_order_get'))
    @method_decorator(validate_parameters(ListCustomerOrderSchema))
    def get(self, request, cleaned_data):
        total, ls = work_order_service.list_work_order(handler_id=request.user_id, **cleaned_data)
        return PaginationResponse(total, [wapper_work_order(e) for e in ls])


# 获取充值申诉
class AppealListView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('appeal_get'))
    @method_decorator(validate_parameters(ListAppealSchema))
    def get(self, request, cleaned_data):
        assert request.pro_id
        total, ls = work_order_service.list_work_order(pro_id=request.pro_id, type_id=ProblemType.RECHARGE,
                                                       **cleaned_data)
        return PaginationResponse(total, [wapper_work_order(e) for e in ls])



