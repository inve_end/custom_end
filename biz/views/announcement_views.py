from django.utils.decorators import method_decorator

from biz.core.thanos.services import announcement_service
from biz.views.announcement_forms import *
from gis.admin.decorators import require_login, check_permission
from gis.common.django_ext.decorators import validate_parameters
from gis.common.django_ext.views import BaseView, PaginationResponse


class AnnouncementListView(BaseView):

    @method_decorator(require_login)
    @method_decorator(check_permission('announcement_get'))
    @method_decorator(validate_parameters(PaginationSchema))
    def get(self, request, cleaned_data):
        assert request.pro_id
        total, ls = announcement_service.list_announcement(pro_id=request.pro_id, **cleaned_data)
        return PaginationResponse(total, ls)

    @method_decorator(require_login)
    @method_decorator(check_permission('announcement_add'))
    @method_decorator(validate_parameters(AddAnnouncementSchema))
    def post(self, request, cleaned_data):
        assert request.pro_id
        return announcement_service.add_announcement(pro_id=request.pro_id, **cleaned_data)


class AnnouncementDetailView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('announcement_get'))
    def get(self, request, data_id):
        data = announcement_service.get_announcement(data_id)
        return data

    @method_decorator(require_login)
    @method_decorator(check_permission('announcement_mod'))
    @method_decorator(validate_parameters(UpdateAnnouncementSchema))
    def put(self, request, data_id, cleaned_data):
        data = announcement_service.update_announcement(data_id=data_id, **cleaned_data)
        return data

    @method_decorator(require_login)
    @method_decorator(check_permission('announcement_del'))
    def delete(self, request, data_id):
        announcement_service.delete_announcement(data_id)
        return dict(id=data_id)

