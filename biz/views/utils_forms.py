from marshmallow import fields

from gis.common.django_ext.forms import BaseSchema, PaginationSchema


class GetFileSchema(BaseSchema):
    url = fields.String(required=True)

