from django.utils.decorators import method_decorator

from biz.core.thanos.services import performance_service
from biz.views.performance_forms import *
from biz.views.helper import wapper_performance
from gis.admin.decorators import require_login, check_permission
from gis.common.django_ext.decorators import validate_parameters
from gis.common.django_ext.views import BaseView, PaginationResponse


class PerformanceListView(BaseView):

    @method_decorator(require_login)
    @method_decorator(check_permission('performance_get'))
    @method_decorator(validate_parameters(PerformanceListSchema))
    def get(self, request, cleaned_data):
        assert request.pro_id
        total, ls = performance_service.list_performance(pro_id=request.pro_id, **cleaned_data)
        return PaginationResponse(total, [wapper_performance(e) for e in ls])


class PerformanceTtlView(BaseView):

    @method_decorator(require_login)
    @method_decorator(check_permission('performance_get'))
    @method_decorator(validate_parameters(PerformanceTtlSchema))
    def get(self, request, cleaned_data):
        assert request.pro_id
        ttl = performance_service.get_ttl_data(pro_id=request.pro_id, **cleaned_data)
        return ttl


class DownLoadView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('performance_download'))
    @method_decorator(validate_parameters(PerformanceTtlSchema))
    def get(self, request, cleaned_data):
        assert request.pro_id
        resp = performance_service.download(pro_id=request.pro_id, **cleaned_data)
        return resp
