from marshmallow import fields

from gis.common.django_ext.forms import BaseSchema, PaginationSchema


class ListLoginOrderSchema(PaginationSchema):
    data_id = fields.Integer()
    net = fields.Integer()
    contact_type = fields.Integer()
    created_at_start = fields.DateTime()
    created_at_end = fields.DateTime()
    status = fields.Integer()


class UpdateLoginOrderSchema(BaseSchema):
    status = fields.Integer()
    remark = fields.String()

