from marshmallow import fields

from gis.common.django_ext.forms import BaseSchema, PaginationSchema


class ListPhraseSchema(PaginationSchema):
    type_id = fields.Integer()


class AddPhraseSchema(BaseSchema):
    type_id = fields.Integer(required=True)
    temp_type = fields.Integer(required=True)
    title = fields.String(required=True)
    context = fields.String(required=True)


class UpdatePhraseSchema(BaseSchema):
    # type_id = fields.Integer(required=True)
    # temp_type = fields.Integer(required=True)
    title = fields.String(required=True)
    context = fields.String(required=True)


class CustomerPhraseSchema(BaseSchema):
    type_id = fields.Integer(required=True)
