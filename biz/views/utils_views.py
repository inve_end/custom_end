from django.utils.decorators import method_decorator

from gis.admin.decorators import require_login
from gis.common.django_ext.views import BaseView
from gis.common.django_ext.decorators import validate_parameters
from biz.views.utils_forms import *
from biz.utils import qiniu_utils


class UploadTokenView(BaseView):
    @method_decorator(require_login)
    def get(self, request):
        token = qiniu_utils.get_token()
        return dict(token=token)


class GetFilePathView(BaseView):
    @method_decorator(require_login)
    @method_decorator(validate_parameters(GetFileSchema))
    def get(self, request, cleaned_data):
        path = qiniu_utils.fix_image_url(**cleaned_data)
        return dict(path=path)
