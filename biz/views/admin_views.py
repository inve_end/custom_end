from django.utils.decorators import method_decorator

from biz.const import Role
from gis.admin.decorators import require_login
from gis.common.django_ext.views import BaseView
from gis.admin.services import admin_service


class GetKefuView(BaseView):
    @method_decorator(require_login)
    def get(self, request):
        assert request.pro_id
        _, ls = admin_service.list_users(pro_id=request.pro_id, page_no=1, page_size=1000, role_id=Role.KEFU)
        return [{'id': e['id'], 'name': e['name']} for e in ls]


class GetKefuAdminView(BaseView):
    @method_decorator(require_login)
    def get(self, request):
        assert request.pro_id
        _, ls = admin_service.list_users(pro_id=request.pro_id, page_no=1, page_size=1000, role_id=Role.KEFU_ADMIN)
        return [{'id': e['id'], 'name': e['name']} for e in ls]


class GetReportAdminView(BaseView):
    @method_decorator(require_login)
    def get(self, request):
        assert request.pro_id
        _, ls1 = admin_service.list_users(pro_id=request.pro_id, page_no=1, page_size=1000, role_id=Role.KEFU)
        _, ls2 = admin_service.list_users(pro_id=request.pro_id, page_no=1, page_size=1000, role_id=Role.CHARGE_YUNYIN)
        ls1 = [{'id': e['id'], 'name': e['name']} for e in ls1]
        ls2 = [{'id': e['id'], 'name': e['name']} for e in ls2]
        return ls1 + ls2
