from gis.admin.services import admin_service
from biz.core.thanos.services import problem_type_service, work_order_service, performance_service
from biz.utils import qiniu_utils
from biz.const import SecondType, ThirdType


def wapper_problem_type(obj):
    if obj.get('type_id'):
        problem_type = problem_type_service.get_problem_type(obj['type_id'])
        obj['type'] = {'desc': problem_type['desc']}
    return obj


def wapper_image_path(obj):
    if obj.get('image'):
        path = qiniu_utils.fix_image_url(url=obj['image'])
        obj['_image'] = obj['image']
        obj['image'] = path
    if obj.get('images'):
        resp_images = []
        for image in obj['images']:
            path = qiniu_utils.fix_image_url(image)
            resp_images.append(path)
        obj['images'] = resp_images
    return obj


def wapper_work_order(obj):
    if obj.get('type_id'):
        problem_type = problem_type_service.get_problem_type(obj['type_id'])
        obj['type'] = {'desc': problem_type['desc']}
    if obj.get('handler_id'):
        handler = admin_service.get_user_by_id(obj['handler_id'])
        obj['handler'] = handler['name']
    if obj.get('images'):
        resp_images = []
        for image in obj['images']:
            path = qiniu_utils.fix_image_url(image)
            resp_images.append(path)
        obj['images'] = resp_images
    if obj.get('pay_id'):
        obj['pay_id'] = str(obj['pay_id'])
    if obj.get('withdraw_id'):
        obj['withdraw_id'] = str(obj['withdraw_id'])
    return obj


def wapper_reassign(obj):
    if obj.get('from_user_id'):
        from_user = admin_service.get_user_by_id(obj['from_user_id'])
        obj['from_user'] = {'name': from_user['name']}
    if obj.get('to_user_id'):
        to_user = admin_service.get_user_by_id(obj['to_user_id'])
        obj['to_user'] = {'name': to_user['name']}
    if obj.get('operator_id'):
        operator = admin_service.get_user_by_id(obj['operator_id'])
        obj['operator'] = {'name': operator['name']}
    return obj


def wapper_user_data(obj, type_id):
    obj['wait_count'] = work_order_service.get_wait_order_count(obj['id'])
    obj['good_percent'] = performance_service.get_yesterday_good_percent(obj['id'], type_id)
    return obj


def wapper_performance(obj):
    if obj.get('type_id'):
        problem_type = problem_type_service.get_problem_type(obj['type_id'])
        obj['type'] = {'desc': problem_type['desc']}
    if obj.get('user_id'):
        user = admin_service.get_user_by_id(obj['user_id'])
        obj['user'] = {'name': user['name']}
    return obj


def wapper_report(obj):
    if obj.get('type_id'):
        problem_type = problem_type_service.get_problem_type(obj['type_id'])
        obj['type'] = {'desc': problem_type['desc']}
    if obj.get('second_type'):
        obj['second_type'] = SecondType.get(obj['second_type'])
    if obj.get('third_type'):
        obj['third_type'] = ThirdType.get(obj['third_type'])
    return obj


def wapper_login_order(obj):
    if obj.get('handler_id'):
        handler = admin_service.get_user_by_id(obj['handler_id'])
        obj['handler'] = handler['name']
    if obj.get('images'):
        resp_images = []
        for image in obj['images']:
            path = qiniu_utils.fix_image_url(image)
            resp_images.append(path)
        obj['images'] = resp_images
    return obj
