from datetime import datetime, timedelta
from marshmallow import fields

from gis.common.django_ext.forms import BaseSchema, PaginationSchema
from biz.views.base import ProSignRequestSchema


class ListCommonProblemSchema(PaginationSchema):
    pro_id = fields.Integer(required=True)
    type_id = fields.Integer(required=True)


class ListAnnouncementSchema(PaginationSchema):
    pro_id = fields.Integer(required=True)
    pkg = fields.String(missing='all')
    chn = fields.String(missing='all')


class ListWorkOrderSchema(PaginationSchema):
    player_id = fields.Integer(required=True)
    created_at_start = fields.DateTime(missing=datetime.now() - timedelta(days=30))


class AddWorkOrderSchema(BaseSchema):
    pro_id = fields.Integer(required=True)
    player_id = fields.Integer(required=True)
    type_id = fields.Integer(required=True)
    level = fields.Integer(required=True)
    chn = fields.String(required=True)
    game_version = fields.String(required=True)
    ua = fields.String(required=True)
    platform = fields.String(required=True)
    game = fields.Integer()
    question_type = fields.Integer()
    appear_at = fields.DateTime()
    images = fields.List(fields.String())
    withdraw_id = fields.Integer()
    withdraw_at = fields.DateTime()
    amount = fields.Float()
    company = fields.String()
    pay_id = fields.Integer()
    pay_type = fields.Integer()
    pay_name = fields.String()
    pay_at = fields.DateTime()
    desc = fields.String()
    cooperation = fields.String()


class OrderCommentSchema(BaseSchema):
    comment = fields.Integer(required=True)


class GetUnCheckedCountSchema(BaseSchema):
    player_id = fields.Integer(required=True)


class GetRecentOrderSchema(BaseSchema):
    type_id = fields.Integer(required=True)
    player_id = fields.Integer(required=True)
    pro_id = fields.Integer(required=True)


class GetFileSchema(BaseSchema):
    url = fields.String(required=True)


class GetTokenSchema(ProSignRequestSchema):
    user_id = fields.Integer(required=True)
    pro_id = fields.Integer(required=True)


class AddLoginOrderSchema(BaseSchema):
    pro_id = fields.Integer(required=True)
    appear_at = fields.DateTime(required=True)
    net = fields.Integer(required=True)
    phone_model = fields.String(required=True)
    contact_type = fields.Integer()
    contact_account = fields.String()
    desc = fields.String()
    images = fields.List(fields.String())

