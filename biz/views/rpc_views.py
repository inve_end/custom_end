from django.utils.decorators import method_decorator

from biz.core.thanos.services import problem_type_service, common_problem_service, announcement_service, \
    work_order_service, rpc_service, login_order_service
from biz.views.rpc_forms import *
from .helper import wapper_image_path, wapper_problem_type, wapper_work_order
from gis.common.django_ext.decorators import validate_parameters
from gis.common.django_ext.views import BaseView, PaginationResponse
from gis.admin.decorators import require_token
from gis.admin.services import admin_service
from biz.utils import qiniu_utils


class ProblemTypeListView(BaseView):
    @method_decorator(require_token)
    @method_decorator(validate_parameters(PaginationSchema))
    def get(self, request, cleaned_data):
        total, ls = problem_type_service.list_problem_type(**cleaned_data)
        return PaginationResponse(total, [wapper_image_path(e) for e in ls])


class CommonProblemListView(BaseView):
    @method_decorator(require_token)
    @method_decorator(validate_parameters(ListCommonProblemSchema))
    def get(self, request, cleaned_data):
        total, ls = common_problem_service.list_common_problem(**cleaned_data)
        return PaginationResponse(total, ls)


class AnnouncementListView(BaseView):
    @method_decorator(require_token)
    @method_decorator(validate_parameters(ListAnnouncementSchema))
    def get(self, request, cleaned_data):
        total, ls = announcement_service.list_announcement(is_flash=True, **cleaned_data)
        return PaginationResponse(total, ls)


class WorkOrderListView(BaseView):
    @method_decorator(require_token)
    @method_decorator(validate_parameters(ListWorkOrderSchema))
    def get(self, request, cleaned_data):
        total, ls = work_order_service.list_work_order(**cleaned_data)
        return PaginationResponse(total, [wapper_problem_type(e) for e in ls])

    @method_decorator(require_token)
    @method_decorator(validate_parameters(AddWorkOrderSchema))
    def post(self, request, cleaned_data):
        return work_order_service.add_work_order(**cleaned_data)


class WorkOrderDeatilView(BaseView):
    @method_decorator(require_token)
    def get(self, request, data_id):
        data = work_order_service.get_rpc_work_order(data_id)
        return wapper_work_order(data)

    @method_decorator(require_token)
    @method_decorator(validate_parameters(OrderCommentSchema))
    def put(self, request, data_id, cleaned_data):
        data = work_order_service.update_work_order(data_id=data_id, **cleaned_data)
        return data


class WorkOrderUrgentView(BaseView):
    @method_decorator(require_token)
    def put(self, request, data_id):
        data = work_order_service.urgent_work_order(data_id)
        return data


class UnCheckedCountView(BaseView):
    @method_decorator(require_token)
    @method_decorator(validate_parameters(GetUnCheckedCountSchema))
    def get(self, request, cleaned_data):
        count = work_order_service.get_unchecked_count(**cleaned_data)
        return dict(count=count)


class GetRecentOrderView(BaseView):
    @method_decorator(require_token)
    @method_decorator(validate_parameters(GetRecentOrderSchema))
    def get(self, request, cleaned_data):
        ls = rpc_service.get_recent_order(**cleaned_data)
        return PaginationResponse(len(ls), ls)


class UploadTokenView(BaseView):
    @method_decorator(require_token)
    def get(self, request):
        token = qiniu_utils.get_token()
        return dict(token=token)


class GetFilePathView(BaseView):
    @method_decorator(require_token)
    @method_decorator(validate_parameters(GetFileSchema))
    def get(self, request, cleaned_data):
        path = qiniu_utils.fix_image_url(**cleaned_data)
        return dict(path=path)


# casiouser获取token的接口，用sign鉴权
class GetTokenView(BaseView):
    @method_decorator(validate_parameters(GetTokenSchema))
    def get(self, request, cleaned_data):
        token = admin_service.set_auth_token(pro_id=cleaned_data['pro_id'], user_id=cleaned_data['user_id'])
        return dict(token=token)


#  登陆申诉的接口
class LoginOrderListView(BaseView):
    @method_decorator(validate_parameters(AddLoginOrderSchema))
    def post(self, request, cleaned_data):
        return login_order_service.add_login_order(**cleaned_data)


class LoginOrderUploadTokenView(BaseView):
    def get(self, request):
        token = qiniu_utils.get_token()
        return dict(token=token)


class LoginOrderGetFilePathView(BaseView):
    @method_decorator(validate_parameters(GetFileSchema))
    def get(self, request, cleaned_data):
        path = qiniu_utils.fix_image_url(**cleaned_data)
        return dict(path=path)
