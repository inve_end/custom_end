from django.utils.decorators import method_decorator

from biz.core.thanos.services import report_service
from biz.views.report_forms import *
from biz.views.helper import wapper_report
from gis.admin.decorators import require_login, check_permission
from gis.common.django_ext.decorators import validate_parameters
from gis.common.django_ext.views import BaseView, PaginationResponse


class ReportListView(BaseView):

    @method_decorator(require_login)
    @method_decorator(check_permission('report_get'))
    @method_decorator(validate_parameters(ReportListSchema))
    def get(self, request, cleaned_data):
        assert request.pro_id
        total, ls = report_service.list_report(pro_id=request.pro_id, **cleaned_data)
        return PaginationResponse(total, [wapper_report(e) for e in ls])


class ReportTtlView(BaseView):

    @method_decorator(require_login)
    @method_decorator(check_permission('report_get'))
    @method_decorator(validate_parameters(ReportTtlSchema))
    def get(self, request, cleaned_data):
        assert request.pro_id
        ttl = report_service.get_ttl_data(pro_id=request.pro_id, **cleaned_data)
        return ttl


class DownLoadView(BaseView):
    @method_decorator(require_login)
    @method_decorator(check_permission('report_download'))
    @method_decorator(validate_parameters(ReportTtlSchema))
    def get(self, request, cleaned_data):
        assert request.pro_id
        resp = report_service.download(pro_id=request.pro_id, **cleaned_data)
        return resp

