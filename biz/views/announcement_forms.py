from marshmallow import fields

from gis.common.django_ext.forms import BaseSchema, PaginationSchema


class AddAnnouncementSchema(BaseSchema):
    start_at = fields.DateTime(required=True)
    end_at = fields.DateTime(required=True)
    pkg = fields.String(required=True)
    chn = fields.String(required=True)
    system = fields.String(required=True)
    title = fields.String(required=True)
    context = fields.String(required=True)


class UpdateAnnouncementSchema(BaseSchema):
    start_at = fields.DateTime(required=True)
    end_at = fields.DateTime(required=True)
    pkg = fields.String(required=True)
    chn = fields.String(required=True)
    system = fields.String(required=True)
    title = fields.String(required=True)
    context = fields.String(required=True)

