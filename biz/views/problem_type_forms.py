from marshmallow import fields

from gis.common.django_ext.forms import BaseSchema, PaginationSchema


class UpdateProblemTypeSchema(BaseSchema):
    button = fields.String(required=True)
    image = fields.String(required=True)
    desc = fields.String(required=True)

