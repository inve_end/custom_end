from marshmallow import fields

from gis.common.django_ext.forms import BaseSchema, PaginationSchema


class ListCommonProblemSchema(PaginationSchema):
    type_id = fields.Integer()


class AddCommonProblemSchema(BaseSchema):
    type_id = fields.Integer(required=True)
    title = fields.String(required=True)
    answer = fields.String(required=True)


class UpdateCommonProblemSchema(BaseSchema):
    type_id = fields.Integer(required=True)
    title = fields.String(required=True)
    answer = fields.String(required=True)

