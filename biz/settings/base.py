import os

import pymysql

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

SECRET_KEY = 'n)-&0h&#yoy9w!36joykw#-hq12vg!63khct0*9c-4@_n@qu1o'

DEBUG = True

ALLOWED_HOSTS = ['*']

INSTALLED_APPS = [
    'biz.core.thanos',
    'gis.admin.apps.AdminConfig',#权限相关
    'django.contrib.sessions',   #会话框架
    'django_extensions',         #扩展django的manage――runscript命令
]

MIDDLEWARE = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'gis.common.django_ext.middleware.ApiMiddleware',
]

ROOT_URLCONF = 'biz.urls'

WSGI_APPLICATION = 'biz.wsgi.application'

#设置 Django默认连接MySQL的方式
pymysql.install_as_MySQLdb()

LANGUAGE_CODE = 'zh-cn'

TIME_ZONE = 'Asia/Chongqing'

USE_I18N = False

USE_L10N = False

USE_TZ = False

SERVICE_ID = 1

# session有效期24小时
SESSION_COOKIE_AGE = 24 * 60 * 60

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '%(levelname)s %(asctime)s %(message)s'
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/var/log/thanos/thanos.log',
            'formatter': 'simple'
        },
    },
    'root': {
        'level': 'DEBUG',
        'handlers': ['console', 'file'],
    }
}


EXPORT_PATH = '/tmp/export_thanos/'

QINIU_KEY_PAIR = ('IoVr0Ec_pPHuL0M6Fk0MhAPYQOgWzIeNVM8TBjIL',
                  'wytlaSz9F3nQVdApF7E8f8iBDV3dJPz6olWPURVw')
QINIU_DOMAIN = 'http://opimg.yiwanshu.com'
CHAT_BUCKET_NAME = 'hj3-yunyin'
